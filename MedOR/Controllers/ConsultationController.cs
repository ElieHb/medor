﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class ConsultationController : Controller
    {
        //
        // GET: /Consultation/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public ConsultationController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        // [Comm.AuthorizeUser(AccessLevel = "Create Consultations")]
        public ActionResult CreateNewConsultation()
        {
            return View();
        }
      
        //[HttpPost]
        //public ActionResult CreateNewConsultation(DataModel.ConsultationModel model)
        //{
        //    if (ModelState.IsValid)
        //    {

        //        //model.CreatedOn = DateTime.Now;
        //        //model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
        //        //var newConsultation = _MedOrSvc.AddNewDoctor(model);            
        //    }

        //    return View(model);
        //}

    }
}
