﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;
using System.Collections;
using System.Globalization;
using Newtonsoft.Json;

using Twilio;
using System.Configuration;

namespace MedOR.Controllers
{
    [Authorize]
    public class CalendarController : Controller
    {
        //
        // GET: /Calendar/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public CalendarController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "View Calendars")]
        public ActionResult ViewCalendar()
        {
            var model = _MedOrSvc.GetCalendarByUserID(new Guid(HttpContext.User.Identity.Name));
            return View(model);
        }

        [Comm.AuthorizeUser(AccessLevel = "View Appointments")]
        public JsonResult GetAppointments(Guid ClinicID)
        {
            var eventList = new List<DataModel.AppointmentModel>(); 
            var appointments = _MedOrSvc.GetAppointmentsByUserIDAndClinicID(new Guid(HttpContext.User.Identity.Name), ClinicID);
            DateTime currStart;
            DateTime currEnd;
            foreach (var appointment in appointments)
            {
                currStart = Convert.ToDateTime(appointment.StartDateTime);
                currEnd = Convert.ToDateTime(appointment.EndDateTime);
                eventList.Add(new DataModel.AppointmentModel()
                {
                    id = appointment.AppointmentID.ToString(),
                    title = appointment.Title,
                    start = currStart.ToString("s"),
                    end = currEnd.ToString("s"),
                    allDay = appointment.AllDay,
                   // url = "/Schedule/Details/" + oc.ID.ToString() + "/"
                });
            }

            var rows = eventList.ToArray();

            return Json(rows, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAppointmentByID(Guid AppointmentID)
        {
            var appointment = _MedOrSvc.GetAppointmentByID(AppointmentID);

            return Json(appointment, JsonRequestBehavior.AllowGet);
        }

        private long ToUnixTimespan(DateTime date)
        {
            TimeSpan tspan = date.ToUniversalTime().Subtract(
                new DateTime(1970, 1, 1, 0, 0, 0));

            return (long)Math.Truncate(tspan.TotalSeconds);
        }

        private static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        [Comm.AuthorizeUser(AccessLevel = "Create Appointments")]
        public JsonResult AddAppointment(String title, String start, String end, String allDay, Guid clinicID, String note, Guid patientID, Guid doctorID)
        {
            // "Tue May 06 2014 00:00:00 GMT+0300 (Middle East Daylight Time)"
            var fstart = start.Split(' ');
            var dtstart = fstart[0] + " " + fstart[1] + " " + fstart[2] + " " + fstart[3] + " " + fstart[4];
            var tzstart = start.Substring(dtstart.Length);
            string sysFormatStart = "ddd MMM dd yyyy HH:mm:ss" + "'" + tzstart + "'";
            var startDate = DateTime.ParseExact(start, sysFormatStart, CultureInfo.InvariantCulture);

            var fend = end.Split(' ');
            var dtend = fend[0] + " " + fend[1] + " " + fend[2] + " " + fend[3] + " " + fend[4];
            var tzend = start.Substring(dtend.Length);
            string sysFormatEnd = "ddd MMM dd yyyy HH:mm:ss" + "'" + tzend + "'";
            var endDate = DateTime.ParseExact(end, sysFormatEnd, CultureInfo.InvariantCulture);
         

            var model = new DataModel.AppointmentModel();
            model.ClinicID = clinicID;
            model.PatientID = patientID;
            model.DoctorID = doctorID;
            model.title = title;
            model.Note = note;
            model.StartDateTime = startDate;
            model.EndDateTime = endDate;
            model.allDay = Convert.ToBoolean(allDay);
            model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
            model.CreatedOn = DateTime.Now;
            var addAppointment = _MedOrSvc.AddNewAppointment(model);


            //send sms
          //   protected void SendMessage_OnClick(object sender, EventArgs e)
           // {
            string ACCOUNT_SID = "AC9238e5060a359699884a46e0a1b92718";// ConfigurationManager.AppSettings["ACCOUNT_SID"];
            string AUTH_TOKEN = "de25b9f300c8dcce940a7d05e5487225";// ConfigurationManager.AppSettings["AUTH_TOKEN"];

            TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

            //+13012462984

            client.SendMessage("(301) 246-2984", "+96171723336", "test");
            //}

    //        var people = new Dictionary<string, string>() {
    //    {"+14158675309","Curious George"},
    //    {"+14158675310","Boots"},
    //    {"+14158675311","Virgil"}
    //};

    //        // iterate over all our friends
    //        foreach (var person in people)
    //        {

    //            // Send a new outgoing SMS by POSTing to the Messages resource */
    //            client.SendMessage(
    //                "YYY-YYY-YYYY", // From number, must be an SMS-enabled Twilio number
    //                person.Key,     // To number, if using Sandbox see note above
    //                // message content
    //                string.Format("Hey {0}, Monkey Party at 6PM. Bring Bananas!", person.Value)
    //            );

    //            Response.Write(string.Format("Sent message to {0}", person.Value));
    //        }


            return Json(addAppointment, JsonRequestBehavior.AllowGet);
        }

        [Comm.AuthorizeUser(AccessLevel = "Edit Appointments")]
        public JsonResult EditAppointment(Guid appointmentID, String title, String start, String end, String allDay, Guid clinicID, String note, Guid patientID, Guid doctorID)
        {
            // "Tue May 06 2014 00:00:00 GMT+0300 (Middle East Daylight Time)"
            var fstart = start.Split(' ');
            var dtstart = fstart[0] + " " + fstart[1] + " " + fstart[2] + " " + fstart[3] + " " + fstart[4];
            var tzstart = start.Substring(dtstart.Length);
            string sysFormatStart = "ddd MMM dd yyyy HH:mm:ss" + "'" + tzstart + "'";
            var startDate = DateTime.ParseExact(start, sysFormatStart, CultureInfo.InvariantCulture);

            var fend = end.Split(' ');
            var dtend = fend[0] + " " + fend[1] + " " + fend[2] + " " + fend[3] + " " + fend[4];
            var tzend = start.Substring(dtend.Length);
            string sysFormatEnd = "ddd MMM dd yyyy HH:mm:ss" + "'" + tzend + "'";
            var endDate = DateTime.ParseExact(end, sysFormatEnd, CultureInfo.InvariantCulture);

            var editAppointment = _MedOrSvc.GetAppointmentByID(appointmentID);
            editAppointment.PatientID = patientID;
            editAppointment.DoctorID = doctorID;
            editAppointment.title = title;
            editAppointment.Note = note;
            editAppointment.StartDateTime = startDate;
            editAppointment.EndDateTime = endDate;
            editAppointment.allDay = Convert.ToBoolean(allDay);
            editAppointment.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
            editAppointment.ModifiedOn = DateTime.Now;
            var updateAppointment = _MedOrSvc.UpdateAppointment(editAppointment);

            return Json(updateAppointment, JsonRequestBehavior.AllowGet);
        }

        [Comm.AuthorizeUser(AccessLevel = "Edit Appointments")]
        public JsonResult EditAppointmentDateTime(Guid appointmentID, String start, String end, String allDay)
        {
            var fstart = start.Split(' ');
            var dtstart = fstart[0] + " " + fstart[1] + " " + fstart[2] + " " + fstart[3] + " " + fstart[4];
            var tzstart = start.Substring(dtstart.Length);
            string sysFormatStart = "ddd MMM dd yyyy HH:mm:ss" + "'" + tzstart + "'";
            var startDate = DateTime.ParseExact(start, sysFormatStart, CultureInfo.InvariantCulture);

            var fend = end.Split(' ');
            var dtend = fend[0] + " " + fend[1] + " " + fend[2] + " " + fend[3] + " " + fend[4];
            var tzend = start.Substring(dtend.Length);
            string sysFormatEnd = "ddd MMM dd yyyy HH:mm:ss" + "'" + tzend + "'";
            var endDate = DateTime.ParseExact(end, sysFormatEnd, CultureInfo.InvariantCulture);

            var model = _MedOrSvc.GetAppointmentByID(appointmentID);
            model.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
            model.ModifiedOn = DateTime.Now;
            model.StartDateTime = startDate;
            model.EndDateTime = endDate;
            model.AllDay = allDay != null ? Convert.ToBoolean(allDay) : false;
            var editAppointment = _MedOrSvc.UpdateAppointmentDateTime(model);

            return Json(editAppointment, JsonRequestBehavior.AllowGet);
        }

        [Comm.AuthorizeUser(AccessLevel = "Delete Appointments")]
        public JsonResult DeleteAppointment(Guid AppointmentID)
        {
            var deleted = _MedOrSvc.DeleteAppointmentByID(AppointmentID);

            return Json(deleted, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult LoadDoctors(Guid? SelectedDoctorID)
        {
            var doctors = _MedOrSvc.GetDoctorsByUserID(new Guid(HttpContext.User.Identity.Name));
            var doctorsList = Comm.GetAvailableDoctorList(doctors, SelectedDoctorID);
            return Json(doctorsList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult LoadPatients(Guid? SelectedPatientID)
        {
            var patients = _MedOrSvc.GetAllPatients(new Guid(HttpContext.User.Identity.Name));
            var patientsList = Comm.GetPatientList(patients, SelectedPatientID);
            return Json(patientsList, JsonRequestBehavior.AllowGet);
        }

    }
}
