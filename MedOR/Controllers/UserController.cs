﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;
using WebMatrix.Data;
using WebMatrix.WebData;
using System.Web.Security;

namespace MedOR.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        //
        // GET: /User/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public UserController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "View Users")]
        public ActionResult ListOfUsers()
        {
            var users = _MedOrSvc.GetAllUsers();
            return View(users);
        }

        [Comm.AuthorizeUser(AccessLevel = "Create Users")]
        public ActionResult CreateNewUser()
        {
            DataModel.UserModel userModel = new DataModel.UserModel();
            userModel = _MedOrSvc.GetAllAccess();
            return View(userModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewUser(DataModel.UserModel model, HttpPostedFileBase uploadFile) 
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!WebSecurity.UserExists(model.Username))
                    {
                        WebSecurity.CreateUserAndAccount(model.Username, model.Password, new { EmailId = model.Email });
                        var userProfileID = _MedOrSvc.GetUserProfileIDByUsername(model.Username);
                        if (userProfileID != 0)
                        {
                            if (uploadFile != null && uploadFile.ContentLength > 0)
                            {
                                model.Image = new byte[uploadFile.ContentLength];
                                uploadFile.InputStream.Read(model.Image, 0, uploadFile.ContentLength);
                            }

                            model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                            model.CreatedOn = DateTime.Now;
                            model.UserProfileID = userProfileID;
                            var newUser = _MedOrSvc.AddNewUser(model);

                            if(newUser != 0)
                                return Redirect("/User/ListOfUsers");
                        }
                    }
                    else 
                    {
                        TempData["MessageError"] = "Username already exists."; 
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    //ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                    throw e;
                }
            }
            model = _MedOrSvc.GetAllAccess();
            return View(model);
        }

        [Comm.AuthorizeUser(AccessLevel = "Edit Users")]
        public ActionResult EditUser(Guid? id)
        {
            if (String.IsNullOrEmpty(id.ToString()) || id == Guid.Empty)
                id = new Guid(HttpContext.User.Identity.Name);
            var model = _MedOrSvc.GetUserByID(id.Value);
            model.ClinicModels = _MedOrSvc.GetAllAccess().ClinicModels;
            model.DoctorModels = _MedOrSvc.GetAllAccess().DoctorModels;
            model.RoleModels = _MedOrSvc.GetAllAccess().RoleModels;

            if (model == null)
            {
                // return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult ShowImage(Guid id)
        {
            var image = _MedOrSvc.GetUserImage(new Guid(id.ToString()));
            return File(image, "image/jpg");
        }
       
        [HttpPost]
        public ActionResult EditUser(DataModel.UserModel model, HttpPostedFileBase uploadFile)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var currentUserId = WebSecurity.GetUserId(model.Username);
                    var userProfileID = _MedOrSvc.GetUserProfileIDByUserID(model.UserID);
                    if ( (currentUserId == userProfileID))
                    { 
                        if (userProfileID != 0)
                        {
                            var editUserProfile = _MedOrSvc.UpdateUserProfile(userProfileID, model.Email);
                            if (editUserProfile != 0)
                            {
                                if (uploadFile != null && uploadFile.ContentLength > 0)
                                {
                                    model.Image = new byte[uploadFile.ContentLength];
                                    uploadFile.InputStream.Read(model.Image, 0, uploadFile.ContentLength);
                                }

                                model.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
                                model.ModifiedOn = DateTime.Now;

                                var editUser = _MedOrSvc.UpdateUser(model);

                                if (editUser != 0)
                                {
                                    return Redirect("/User/ListOfUsers");
                                }
                            }
                        }
                    }
                }
                catch (MembershipCreateUserException e)

                {
                    //ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                    throw e;
                }
            }
            model.ClinicModels = _MedOrSvc.GetAllAccess().ClinicModels;
            model.DoctorModels = _MedOrSvc.GetAllAccess().DoctorModels;
            model.RoleModels = _MedOrSvc.GetAllAccess().RoleModels;
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(String OldPassword, String Password, Guid userID, String userName)
        {
            if (ModelState.IsValid)
            {
                if (!String.IsNullOrEmpty(OldPassword))
                {
                    MembershipUser u = Membership.GetUser(userName);

                    try
                    {
                        if (u.ChangePassword(OldPassword, Password))
                        {
                            TempData["MessageSuccess"] = "Password successfully changed.";
                            return Redirect("/User/EditUser?" + userID);
                            //return Redirect("/User/ListOfUsers");
                        }
                        else
                        {
                            TempData["MessageError"] = "Password change failed. Please re-enter your values and try again.";
                        }
                    }
                    catch (Exception e)
                    {
                        TempData["MessageError"] = "An exception occurred: " + Server.HtmlEncode(e.Message) + ". Please re-enter your values and try again.";
                    }
                }
            }
            return Redirect("/User/EditUser?" + userID);
        }

        [Comm.AuthorizeUser(AccessLevel = "Delete Users")]
        public ActionResult DeleteUser(Guid id)
        {
            var username = _MedOrSvc.GetUsernameByUserID(id);
            var deleted = _MedOrSvc.DeleteUserByID(id);
            if (deleted != 0)
            {
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(username); // deletes record from webpages_Membership table
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser(username, true); // deletes record from UserProfile table
                return Redirect("/User/ListOfUsers");
            }
            return View();
        }
    }
}
