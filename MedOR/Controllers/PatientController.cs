﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class PatientController : Controller
    {
        //
        // GET: /Patient/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public PatientController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "View Patients")]
        public ActionResult ListOfPatients()
        {
            var patients = _MedOrSvc.GetAllPatients(new Guid(HttpContext.User.Identity.Name));
            return View(patients);
        }

        [Comm.AuthorizeUser(AccessLevel = "Create Patients")]
        public ActionResult CreateNewPatient()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewPatient(DataModel.PaitentModel model, HttpPostedFileBase uploadFile)
        {
            if (ModelState.IsValid)
            {
                if (uploadFile != null && uploadFile.ContentLength > 0)
                {
                    model.Image = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(model.Image, 0, uploadFile.ContentLength);
                }

                model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                model.CreatedOn = DateTime.Now;
                model.DateOfBirth = new DateTime(Convert.ToInt32(model.YYYY), Convert.ToInt32(model.MM), Convert.ToInt32(model.DD));

                var newUser = _MedOrSvc.AddNewPatient(model);

                if (newUser != 0)
                    return Redirect("/Patient/ListOfPatients");
            }
            return View(model);
        }

        public ActionResult ShowPatientImage(Guid id)
        {
            var image = _MedOrSvc.GetPatientImage(new Guid(id.ToString()));
            return File(image, "image/jpg");
        }

        [Comm.AuthorizeUser(AccessLevel = "Edit Patients")]
        public ActionResult EditPatient(Guid? id)
        {
            if (String.IsNullOrEmpty(id.ToString()) || id == Guid.Empty)
                id = new Guid(HttpContext.User.Identity.Name);
            var model = _MedOrSvc.GetPatientByID(id.Value);
            model.DD = model.DateOfBirth.Day.ToString();
            model.MM = model.DateOfBirth.Month.ToString();
            model.YYYY = model.DateOfBirth.Year.ToString();

            if (model == null)
            {
                //return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EditPatient(DataModel.PaitentModel model, HttpPostedFileBase uploadFile)
        {
            if (ModelState.IsValid)
            {
                if (uploadFile != null && uploadFile.ContentLength > 0)
                {
                    model.Image = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(model.Image, 0, uploadFile.ContentLength);
                }

                model.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                model.DateOfBirth = new DateTime(Convert.ToInt32(model.YYYY), Convert.ToInt32(model.MM), Convert.ToInt32(model.DD));
                var editPatient = _MedOrSvc.UpdatePatient(model);

                if (editPatient != 0)
                    return Redirect("/Patient/ListOfPatients");
            }
            return View(model);
        }

        [Comm.AuthorizeUser(AccessLevel = "Delete Patients")]
        public ActionResult DeletePatient(Guid id)
        {
            var deleted = _MedOrSvc.DeletePatientByID(id);
            if (deleted != 0)
                return Redirect("/Patient/ListOfPatients");
            return View();
        }
    }
}