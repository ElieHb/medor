﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class ClinicController : Controller
    {
        //
        // GET: /Clinic/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public ClinicController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "View Clinics")]
        public ActionResult ListOfClinics()
        {
            var clinics = _MedOrSvc.GetClinicsByUserID(new Guid(HttpContext.User.Identity.Name));
            return View(clinics);
        }

        [Comm.AuthorizeUser(AccessLevel = "Create Clinics")]
        public ActionResult CreateNewClinic()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewClinic(DataModel.ClinicModel model) 
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                var newClinic = _MedOrSvc.AddNewClinic(model);

                if (newClinic != 0)
                    return Redirect("/Clinic/ListOfClinics");
            }
            return View(model);
        }

        [Comm.AuthorizeUser(AccessLevel = "Edit Clinics")]
        public ActionResult EditClinic(Guid id)
        {
            var model = _MedOrSvc.GetClinicByID(id);
            if (model == null)
            {
               // return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EditClinic(DataModel.ClinicModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                var editClinic = _MedOrSvc.UpdateClinic(model);

                if (editClinic != 0)
                    return Redirect("/Clinic/ListOfClinics");
            }
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "Delete Clinics")]
        public ActionResult DeleteClinic(Guid id)
        {
            var deleted = _MedOrSvc.DeleteClinicByID(id);
            if (deleted != 0)
                return Redirect("/Clinic/ListOfClinics");
            return View();
        }
    }
}
