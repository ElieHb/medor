﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Web.Security;
using WebMatrix.WebData;
using WebMatrix.Data;


namespace MedOR.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/

        #region Constructor
        private DataServices.Service _MedOrSvc;
        Global.SessionContext context;

        public AccountController()
        {
            _MedOrSvc = new DataServices.Service();
            context = new Global.SessionContext();
        }
        #endregion

        public ActionResult Login()
        {
            context = new Global.SessionContext();
            return View();
        }

        [HttpPost]
        public ActionResult Login(DataModel.UserModel userModel)
        {
            if (Membership.ValidateUser(userModel.Username, userModel.Password))
            {
                // Log the user into the site

                var userProfileID = _MedOrSvc.GetUserProfileIDByUsername(userModel.Username);
                var authenticatedUser = _MedOrSvc.GetUserByUserProfileID(userProfileID);

                context.SetAuthenticationToken(authenticatedUser.UserID.ToString(), false, authenticatedUser);
                authenticatedUser.LastLoggedIn = DateTime.Now;
                authenticatedUser.ModifiedBy = authenticatedUser.UserID;
                authenticatedUser.ModifiedOn = DateTime.Now;
                var saveLoggedIn = _MedOrSvc.UpdateUserLastLoggedIn(authenticatedUser);

                return RedirectToAction("Index", "Home");
            }
            // If we reach here, the user's credentials were invalid
            TempData["Message"] = "Invalide user credentials";

            return View();
        }

        public ActionResult LockScreen()
        {
            FormsAuthentication.SignOut();

            DataModel.UserModel model = new DataModel.UserModel();
            if (!String.IsNullOrEmpty(HttpContext.User.Identity.Name))
            {
                var userID = new Guid(HttpContext.User.Identity.Name);

                if (userID != null)
                {
                    model = _MedOrSvc.GetUserByIDForLockScreen(userID);
                }
            }
            
            return View(model);
        }

        [HttpPost]
        public ActionResult LockScreen(DataModel.UserModel userModel)
        {
            if (userModel.Username != null && userModel.Password != null)
            {
                if (Membership.ValidateUser(userModel.Username, userModel.Password))
                {
                    var userProfileID = _MedOrSvc.GetUserProfileIDByUsername(userModel.Username);
                    var authenticatedUser = _MedOrSvc.GetUserByUserProfileID(userProfileID);

                    context.SetAuthenticationToken(authenticatedUser.UserID.ToString(), false, authenticatedUser);
                    return RedirectToAction("Index", "Home");
                }
            }

            return View(userModel);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Account");
        }

        public ActionResult ForgotPassword(String username)
        {
            DataModel.UserModel userModel = new DataModel.UserModel();
            userModel.Username = username;
            return View(userModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string userName, String email)
        {
            //check user existance
            var user = Membership.GetUser(userName);
            if (user == null)
            {
                TempData["Message"] = "User Does Not Exist.";
            }
            else
            {
                //generate password token
                var token = WebSecurity.GeneratePasswordResetToken(userName);
                //create url with above token
                var resetLink = "<a href='" + Url.Action("ResetPassword", "Account", new { un = userName, rt = token }, "http") + "'>Reset Password</a>";
                //get user email
                var emailid = _MedOrSvc.GetEmailByUsername(userName);
                if (emailid == email)
                {
                    //send mail
                    string subject = "Password Reset Token";
                    string body = "<b>Please find the Password Reset Token</b><br/>" + resetLink; //edit it
                    try
                    {
                        SendEMail(email, subject, body);
                        TempData["Message"] = "Mail Sent.";
                    }
                    catch (Exception ex)
                    {
                        TempData["Message"] = "Error occured while sending email." + ex.Message;
                    }
                    //only for testing
                    //TempData["Message"] = resetLink;
                }
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string un, string rt)
        {
            //TODO: Check the un and rt matching and then perform following
            //get userid of received username
            var userid = _MedOrSvc.GetUserProfileIDByUsername(un);
            //check userid and token matches
            bool any = _MedOrSvc.CheckMatchUserIDAndToken(userid, rt); 

            if (any == true)
            {
                //generate random password
                string newpassword = GenerateRandomPassword(6);
                //reset password
                bool response = WebSecurity.ResetPassword(rt, newpassword);
                if (response == true)
                {
                    //get user emailid to send password
                    var emailid = _MedOrSvc.GetEmailByUsername(un);
                    //send email
                    string subject = "New Password";
                    string body = "<b>Please find the New Password</b><br/>" + newpassword; //edit it
                    try
                    {
                        SendEMail(emailid, subject, body);
                        TempData["Message"] = "Mail Sent.";
                    }
                    catch (Exception ex)
                    {
                        TempData["Message"] = "Error occured while sending email." + ex.Message;
                    }

                    //display message
                    TempData["Message"] = "Success! Check email we sent. Your New Password Is " + newpassword;
                }
                else
                {
                    TempData["Message"] = "Hey, avoid random request on this page.";
                }
            }
            else
            {
                TempData["Message"] = "Username and token not maching.";
            }

                return View();
        }

        private string GenerateRandomPassword(int length)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-*&#+";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        private void SendEMail(string emailid, string subject, string body)
        {
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.live.com"; //mail.stellarissolutions.com
            client.Port = 25;


            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("nancy_ae@live.com", "Softeng_27");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("nancy_ae@live.com");
            msg.To.Add(new MailAddress(emailid));

            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;

            client.Send(msg);
        }
    }
}
