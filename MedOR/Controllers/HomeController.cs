﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        //
        // GET: /Main/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public HomeController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View(); 
        }

        [ChildActionOnly]
        public PartialViewResult _UserInfo()
        {
            var model = _MedOrSvc.GetUserByID(new Guid(HttpContext.User.Identity.Name));
            return PartialView(model);
        }

        public JsonResult SaveLayoutStyle(DataModel.LayoutStyleViewModel layoutStyleViewModel)
        {
            int success = 0;
            if (layoutStyleViewModel != null)
            {
                DataModel.LayoutStyleModel layoutModel = new DataModel.LayoutStyleModel();
                layoutModel.UserID = new Guid(HttpContext.User.Identity.Name);
                layoutModel.Rtl = layoutStyleViewModel.rtl == "False" ? "ltr" : "rtl";
                layoutModel.Layout = layoutStyleViewModel.layoutBoxed == "False" ? "default" : "boxed";
                layoutModel.Header = layoutStyleViewModel.headerDefault == "False" ? "fixed" : "default";
                layoutModel.Footer = layoutStyleViewModel.footerDefault == "False" ? "fixed" : "default";
                layoutModel.Background = layoutStyleViewModel.bgStyle;
                layoutModel.Basic = layoutStyleViewModel.useLess == "True" ? layoutStyleViewModel.baseColor : null;
                layoutModel.Text = layoutStyleViewModel.useLess == "True" ? layoutStyleViewModel.textColor : null;
                layoutModel.Elements = layoutStyleViewModel.useLess == "True" ? layoutStyleViewModel.badgeColor : null;
                layoutModel.PredefinedColorScheme = layoutStyleViewModel.useLess == "False" ? layoutStyleViewModel.skinClass.Split('/')[4].Substring(6).Split('.')[0] : null;

                success = _MedOrSvc.UpdateLayoutStyle(layoutModel);
            }

            return Json(success, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLayoutStyle()
        {
            var layoutStyle = _MedOrSvc.GetLayoutStyleByUserID(new Guid(HttpContext.User.Identity.Name));
            DataModel.LayoutStyleViewModel layoutStyleViewModel = new DataModel.LayoutStyleViewModel();

            if (layoutStyle != null)
            {
                layoutStyleViewModel.rtl = layoutStyle.Rtl == "rtl" ? "True" : "False";
                layoutStyleViewModel.layoutBoxed = layoutStyle.Layout == "default" ? "False" : "True";
                layoutStyleViewModel.headerDefault = layoutStyle.Header == "fixed" ? "False" : "True";
                layoutStyleViewModel.footerDefault = layoutStyle.Footer == "fixed" ? "False" : "True";
                layoutStyleViewModel.bgStyle = layoutStyle.Background;
                layoutStyleViewModel.useLess = layoutStyle.Basic != null ? "True" : "False";
                layoutStyleViewModel.baseColor = layoutStyle.Basic;
                layoutStyleViewModel.textColor = layoutStyle.Text;
                layoutStyleViewModel.badgeColor = layoutStyle.Elements;
                layoutStyleViewModel.skinClass = "../Content/assets/css/theme_" + layoutStyle.PredefinedColorScheme + ".css";
            }

            return Json(layoutStyleViewModel, JsonRequestBehavior.AllowGet);
        }

        [ChildActionOnly]
        public PartialViewResult _CreateModify(String TableName)
        {
            var model = _MedOrSvc.GetViewCreateModifyByUserAndTableName(new Guid(HttpContext.User.Identity.Name), TableName);
            if (model == null)
            {
                model = new DataModel.ViewCreateModifyModel();
                model.TableName = TableName;
                model.ViewCreatedBy = false;
                model.ViewCreatedOn = false;
                model.ViewModifiedBy = false;
                model.ViewModifiedOn = false;
            }
            return PartialView(model);
        }

        public JsonResult SaveView(DataModel.CreateModifyViewTableModel viewCreateModifyModel)
        {
            var viewCreateModifyExist = _MedOrSvc.GetViewCreateModifyExist(new Guid(HttpContext.User.Identity.Name), viewCreateModifyModel.table);
            int newView;
            if (viewCreateModifyExist)
            {
                var model = new DataModel.ViewCreateModifyModel();
                model.UserID = new Guid(HttpContext.User.Identity.Name);
                model.TableName = viewCreateModifyModel.table;
                model.ViewCreatedBy = viewCreateModifyModel.vcreatedBy;
                model.ViewCreatedOn = viewCreateModifyModel.vcreatedOn;
                model.ViewModifiedBy = viewCreateModifyModel.vmodifiedBy;
                model.ViewModifiedOn = viewCreateModifyModel.vmodifiedOn;
                newView = _MedOrSvc.UpdateCreateModify(model);
            }
            else
            {
                var model = new DataModel.ViewCreateModifyModel();
                model.UserID = new Guid(HttpContext.User.Identity.Name);
                model.TableName = viewCreateModifyModel.table;
                model.ViewCreatedBy = viewCreateModifyModel.vcreatedBy;
                model.ViewCreatedOn = viewCreateModifyModel.vcreatedOn;
                model.ViewModifiedBy = viewCreateModifyModel.vmodifiedBy;
                model.ViewModifiedOn = viewCreateModifyModel.vmodifiedOn;
                newView = _MedOrSvc.AddNewCreateModify(model);
            }

            return Json(newView, JsonRequestBehavior.AllowGet);
        }
    }
}
