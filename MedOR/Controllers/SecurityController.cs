﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class SecurityController : Controller
    {
        //
        // GET: /Security/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public SecurityController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "View Roles")]
        public ActionResult ListOfRoles()
        {
            var userId = HttpContext.User.Identity.Name;
            var roles = _MedOrSvc.GetRoleAndPermissions(new Guid(userId));
            return View(roles);
        }

        [HttpPost]
        public ActionResult ListOfRoles(int i)
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "Create Roles")]
        public ActionResult CreateNewRole()
        {
            var model = _MedOrSvc.GetAllPermissionsAndUsersForRoleModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateNewRole(DataModel.RoleModel model) 
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                var newSecurity = _MedOrSvc.AddNewSecurityRole(model);

                if (newSecurity != 0)
                    return Redirect("/Security/ListOfRoles");
            }
            model = _MedOrSvc.GetAllPermissionsAndUsersForRoleModel();
            return View(model);
        }

        [Comm.AuthorizeUser(AccessLevel = "Edit Roles")]
        public ActionResult EditRole(Guid id)
        {
            var model = _MedOrSvc.GetRoleByID(id);
            model.PermissionModels = _MedOrSvc.GetAllPermissionsForRoleModel(id);
            if (model == null)
            {
               // return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EditRole(DataModel.RoleModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                var updateRole = _MedOrSvc.UpdateSecurityRole(model);

                if (updateRole != 0)
                    return Redirect("/Security/ListOfRoles");
            }
            return View(model);
        }

        [Comm.AuthorizeUser(AccessLevel = "Delete Roles")]
        public ActionResult DeleteRole(Guid id)
        {
            var deleted = _MedOrSvc.DeleteRoleByID(id);
            if (deleted != 0)
                return Redirect("/Security/ListOfRoles");
            return View();
        }
    }
}
