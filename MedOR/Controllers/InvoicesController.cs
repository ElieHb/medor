﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;
using WebMatrix.Data;
using WebMatrix.WebData;
using System.Web.Security;

namespace MedOR.Views.Invoices
{
    public class InvoicesController : Controller
    {
        //
        // GET: /Invoices/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public InvoicesController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewInvoices(DataModel.UserModel model)
        {

            DataModel.UserModel userModel = new DataModel.UserModel();
            userModel = _MedOrSvc.GetAllAccess();

            var Invoices = _MedOrSvc.AddNewItemInvoice(model);

            return View(userModel);

}

        public ActionResult AddItem()
        {
          //if (model.InvoicesModels!=null)
          //  //    return Redirect("/Invoices/ViewInvoices?" + model);
          //  return RedirectToAction("ViewInvoices", model);

            return View();
        }
    }
}
