var Calendar = function () {
    //function to initiate Full CAlendar
    var runCalendar = function () {
        var $modal = $('#event-management');
        var $modalConfirm = $('#action-confirm');
        $('#event-categories div.event-category').each(function () {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 50 //  original position after the drag
            });
        });
        /* initialize the calendar
        -----------------------------------------------------------------*/
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var form = '';
        var clinicID = $('#clinic').val();

        var calendar = $('#calendar').fullCalendar({
            buttonText: {
                prev: '<i class="fa fa-chevron-left"></i>',
                next: '<i class="fa fa-chevron-right"></i>'
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: "/Calendar/GetAppointments?ClinicID=" + clinicID,
            //            events: [{
            //                title: 'Meeting with Boss',
            //                start: new Date(y, m, 1),
            //                className: 'label-default'
            //            }, {
            //                title: 'Bootstrap Seminar',
            //                start: new Date(y, m, d - 5),
            //                end: new Date(y, m, d - 2),
            //                className: 'label-teal'
            //            }, {
            //                title: 'Lunch with Nicole',
            //                start: new Date(y, m, d - 3, 12, 0),
            //                className: 'label-green',
            //                allDay: false
            //            }],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                var $categoryClass = $(this).attr('data-class');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                if ($categoryClass)
                    copiedEventObject['className'] = [$categoryClass];
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            selectable: true,
            selectHelper: true,
            select: function (start, end, allDay) {
                $modal.modal({
                    backdrop: 'static'
                });

                var zp = function (val) {
                    return (val <= 9 ? '0' + val : '' + val);
                }

                var zh = function (hours) {
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    return zp(hours);
                }

                function GetStartCurrentTime(d) {
                    var hours = d.getHours();
                    var minutes = d.getMinutes();
                    var ampm = hours >= 12 ? 'PM' : 'AM';
                    return zh(hours) + ":" + zp(minutes) + " " + ampm;
                }

                function GetEndCurrentTime(d) {
                    var newEndDateTime = d.setMinutes(d.getMinutes() + 10);
                    var t = new Date(newEndDateTime);
                    var hours = t.getHours();
                    var minutes = t.getMinutes();
                    var ampm = hours >= 12 ? 'PM' : 'AM';
                    return zh(hours) + ":" + zp(minutes) + " " + ampm;
                }

                var sDate = zp((start.getMonth() + 1)) + "-" + zp(start.getDate()) + "-" + start.getFullYear();
                var hours = start.getHours();
                var minutes = start.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                sTime = zh(hours) + ":" + zp(minutes) + " " + ampm;

                // if (!allDay) {
                //    var newDateTime = start.setMinutes(start.getMinutes() + 30);
                //    end = new Date(newDateTime);
                //}

                var eDate = zp((end.getMonth() + 1)) + "-" + zp(end.getDate()) + "-" + end.getFullYear();
                var hours = end.getHours();
                var minutes = end.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                eTime = zh(hours) + ":" + zp(minutes) + " " + ampm;

                var checked = "";

                if (allDay) {
                    checked = "checked";
                }

                var view = $('#calendar').fullCalendar('getView');
                if (view.name == 'month') {
                    //round current time to the nearest 5 minutes
                    var coeff = 1000 * 60 * 5;
                    var date = new Date(Date.now());  //or use any other date
                    var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
                    sTime = GetStartCurrentTime(rounded);
                    eTime = GetEndCurrentTime(rounded);
                }

                form = $("<form></form>");
                form.append("<div class='row'></div>");
                form.find(".row").append("<div class='col-md-6'><div class='form-group'><label class='control-label' for='form-field-select-1'>Doctor</label><select id='form-field-select-1' class='form-control search-select' name='categoryD'></select></div></div>")
                                 .append("<div class='col-md-6'><div class='form-group'><label class='control-label' for='form-field-select-2'>Patient</label><select id='form-field-select-2' class='form-control search-select' name='categoryP'></select></div></div>")
                                 .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>Start Date</label><div class='input-group'><input type='text' id='startDate' name='startDate' data-date-format='mm-dd-yyyy' value='" + sDate + "' data-date-viewmode='years' class='form-control date-picker'><span class='input-group-addon'><i class='fa fa-calendar'></i></span></div></div></div>")
                                 .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>Start Time</label><div class='input-group input-append bootstrap-timepicker'><input type='text' id='startTime' name='startTime' value='" + sTime + "' class='form-control time-picker'><span class='input-group-addon add-on'><i class='fa fa-clock-o'></i></span></div></div></div>")
                                 .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Title</label><input class='form-control' type=text name='title'/></div></div>")
                                 .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>End Date</label><div class='input-group'><input type='text' id='endDate' name='endDate' data-date-format='mm-dd-yyyy' value='" + eDate + "' data-date-viewmode='years' class='form-control date-picker'><span class='input-group-addon'><i class='fa fa-calendar'></i></span></div></div></div>")
                                 .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>End Time</label><div class='input-group input-append bootstrap-timepicker'><input type='text' id='endTime' name='endTime' value='" + eTime + "' class='form-control time-picker'><span class='input-group-addon add-on'><i class='fa fa-clock-o'></i></span></div></div></div>")
                                 .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Note</label><textarea class='form-control' id='form-field-22' name='note'></textarea></div></div>")
                                 .append("<div class='col-md-6'><div class='form-group'><label class='checkbox-inline'><input type='checkbox' id='isAllDay' name='isAllDay' class='grey'>All Day</label></div></div>");

                $.ajax({
                    "url": "/Calendar/LoadDoctors/",
                    "type": "get",
                    "dataType": "json",
                    "success": function (data) {
                        $("#form-field-select-1").empty();
                        debugger;
                        $.each(data, function () {

                            if (Object.keys(data).length == 1) {
                                $("#form-field-select-1").append($("<option selected/>").val(this.Value).text(this.Text)).find("select[name='categoryD']");
                                $("#form-field-select-1").select2("val", this.Value);
                            }
                            else
                                $("#form-field-select-1").append($("<option />").val(this.Value).text(this.Text)).find("select[name='categoryD']");
                        });
                    }
                });

                $.ajax({
                    "url": "/Calendar/LoadPatients/",
                    "type": "get",
                    "dataType": "json",
                    "success": function (data) {
                        $("#form-field-select-2").empty();

                        $.each(data, function () {
                            $("#form-field-select-2").append($("<option />").val(this.Value).text(this.Text)).find("select[name='categoryP']");
                        });
                    }
                });

                $modal.find('.remove-event').hide().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').click(function () {
                    form.submit();
                });

                $modal.append("<script type='text/javascript' src='../Content/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'></script>");
                $modal.append("<script type='text/javascript' src='../Content/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'></script>");
                $modal.append("<script type='text/javascript' src='../Content/assets/plugins/select2/select2.min.js'></script>");
                $modal.append("<script type='text/javascript' src='../Content/assets/js/form-elements.js'></script>");
                $modal.append("<script type='text/javascript'>jQuery(document).ready(function () {  Main.init(); FormElements.init(); if ($('input.grey').is(':checked')) { $('#startTime').attr('disabled', true); $('#startTime').val('12:00 AM'); $('#endTime').attr('disabled', true); $('#endTime').val('12:00 AM'); }       $('input').on('ifChecked', function () { $('#startTime').attr('disabled', true);  $('#startTime').val('12:00 AM'); $('#endTime').attr('disabled', true);  $('#endTime').val('12:00 AM'); });    $('input').on('ifUnchecked', function () {  $('#startTime').attr('disabled', false); $('#startTime').val(sTime); $('#endTime').attr('disabled', false); $('#endTime').val(eTime); });});  </script>");

                $modal.find('form').on('submit', function () {
                    title = form.find("input[name='title']").val();
                    note = form.find("textarea[name='note']").val();
                    $patientID = form.find("select[name='categoryP'] option:checked").val();
                    $doctorID = form.find("select[name='categoryD'] option:checked").val();
                    isAllDay = form.find("input[name='isAllDay']").is(':checked');
                    startDate = form.find("input[name='startDate']").val();
                    startTime = form.find("input[name='startTime']").val();
                    endDate = form.find("input[name='endDate']").val();
                    endTime = form.find("input[name='endTime']").val();

                    allDay = isAllDay;
                    start = new Date(startDate + " " + startTime);
                    end = new Date(endDate + " " + endTime);

                    $categoryClass = form.find("select[name='category'] option:checked").val();
                    var clinicID = $('#clinic').val();
                    if (title !== null) {

                        // calendar.fullCalendar('ignoreTimezone', false);

                        $.ajax({
                            url: "/Calendar/AddAppointment",
                            type: "GET",
                            cache: false,
                            data: { title: title, start: start, end: end, allDay: allDay, clinicID: clinicID, note: note, patientID: $patientID, doctorID: $doctorID },
                            error: function (xhr, status, error) {
                            },
                            success: function (data) {
                                calendar.fullCalendar('renderEvent', {
                                    title: title,
                                    start: start,
                                    end: end,
                                    allDay: allDay,
                                    className: $categoryClass
                                } 
                                );
                                $('#calendar').fullCalendar('refetchEvents');
                            }
                        });
                    }
                    $modal.modal('hide');
                    return false;
                });
                calendar.fullCalendar('unselect');
            },
            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
                $modalConfirm.modal({
                    backdrop: 'static'
                });

                message = allDay ? "<p><b>" + event.title + "</b> has been moved to an all day appointment.</p>" : "<p>The end date of <b>" + event.title + "</b> has been moved " + dayDelta + " day(s) and " + minuteDelta + " minute(s).</p>";

                form = $("<form></form>");
                form.append("<div class='row'></div>")
                     .append(message);

                $modalConfirm.find('.modal-body').empty().prepend(form).end().find('.btn-primary').unbind('click').click(function () {
                    $.ajax({
                        url: "/Calendar/EditAppointmentDateTime",
                        type: "POST",
                        cache: false,
                        data: { appointmentID: event.id, start: event.start, end: event.end, allDay: allDay },
                        error: function (xhr, status, error) {
                        },
                        success: function (data) {
                        }
                    });
                    $modalConfirm.modal('hide');
                });

                $modalConfirm.find('.modal-body').empty().prepend(form).end().find('.btn-default').unbind('click').click(function () {
                    revertFunc();
                });
            },
            eventResize: function (event, dayDelta, minuteDelta, revertFunc) {
                $modalConfirm.modal({
                    backdrop: 'static'
                });

                form = $("<form></form>");
                form.append("<div class='row'></div>")
                     .append("<p>The end date of <b>" + event.title + "</b> has been moved " + dayDelta + " day(s) and " + minuteDelta + " minute(s).</p>");

                $modalConfirm.find('.modal-body').empty().prepend(form).end().find('.btn-primary').unbind('click').click(function () {
                    $.ajax({
                        url: "/Calendar/EditAppointmentDateTime",
                        type: "POST",
                        cache: false,
                        data: { appointmentID: event.id, start: event.start, end: event.end },
                        error: function (xhr, status, error) {
                        },
                        success: function (data) {
                        }
                    });
                    $modalConfirm.modal('hide');
                });

                $modalConfirm.find('.modal-body').empty().prepend(form).end().find('.btn-default').unbind('click').click(function () {
                    revertFunc();
                });
            },
            eventClick: function (calEvent, jsEvent, view) {
                var form = $("<form></form>");

                var zp = function (val) {
                    return (val <= 9 ? '0' + val : '' + val);
                }

                var zh = function (hours) {
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    return zp(hours);
                }

                function ToJavaScriptDate(value) {
                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(value);
                    var dt = new Date(parseFloat(results[1]));
                    return zp(dt.getMonth() + 1) + "-" + zp(dt.getDate()) + "-" + dt.getFullYear();
                }

                function ToJavaScriptTime(value) {
                    var pattern = /Date\(([^)]+)\)/;
                    var results = pattern.exec(value);
                    var dt = new Date(parseFloat(results[1]));
                    var ampm = dt.getHours() >= 12 ? 'PM' : 'AM';
                    return zh(dt.getHours()) + ":" + zp(dt.getMinutes()) + " " + ampm;
                }

                function GetStartCurrentTime(d) {
                    var hours = d.getHours();
                    var minutes = d.getMinutes();
                    var ampm = hours >= 12 ? 'PM' : 'AM';
                    return zh(hours) + ":" + zp(minutes) + " " + ampm;
                }

                function GetEndCurrentTime(d) {
                    var newEndDateTime = d.setMinutes(d.getMinutes() + 10);
                    var t = new Date(newEndDateTime);
                    var hours = t.getHours();
                    var minutes = t.getMinutes();
                    var ampm = hours >= 12 ? 'PM' : 'AM';
                    return zh(hours) + ":" + zp(minutes) + " " + ampm;
                }
          
                $.ajax({
                   
                    url: "/Calendar/GetAppointmentByID",
                    type: "GET",
                    data: "&AppointmentID=" + calEvent._id,
                    success: function (data) {
                        sDate = ToJavaScriptDate(data.StartDateTime);
                        sTime = ToJavaScriptTime(data.StartDateTime);
                        eDate = ToJavaScriptDate(data.EndDateTime);
                        eTime = ToJavaScriptTime(data.EndDateTime);
                        title = data.Title;
                        note = data.Note
                        checked = data.AllDay ? "checked" : null;

                        //round current time to the nearest 5 minutes
                        var coeff = 1000 * 60 * 5;
                        var date = new Date(Date.now());  //or use any other date
                        var rounded = new Date(Math.round(date.getTime() / coeff) * coeff);
                        startCurrentTime = GetStartCurrentTime(rounded);
                        endCurrentTime = GetEndCurrentTime(rounded);

                        form.append("<div class='row'></div>");
                        form.find(".row").append("<div class='col-md-6'><div class='form-group'><label class='control-label' for='form-field-select-1'>Doctor</label><select id='form-field-select-1' class='form-control search-select' name='categoryD'></select></div></div>")
                        .append("<div class='col-md-6'><div class='form-group'><label class='control-label' for='form-field-select-2'>Patient</label><select id='form-field-select-2' class='form-control search-select' name='categoryP'></select></div></div>")
                        .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>Start Date</label><div class='input-group'><input type='text' id='startDate' name='startDate' data-date-format='mm-dd-yyyy' value='" + sDate + "' data-date-viewmode='years' class='form-control date-picker'><span class='input-group-addon'><i class='fa fa-calendar'></i></span></div></div></div>")
                        .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>Start Time</label><div class='input-group input-append bootstrap-timepicker'><input type='text' id='startTime' name='startTime' value='" + sTime + "' class='form-control time-picker'><span class='input-group-addon add-on'><i class='fa fa-clock-o'></i></span></div></div></div>")
                        .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Title</label><input class='form-control' value='" + title + "' type=text name='title'/></div></div>")
                        .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>End Date</label><div class='input-group'><input type='text' id='endDate' name='endDate' data-date-format='mm-dd-yyyy' value='" + eDate + "' data-date-viewmode='years' class='form-control date-picker'><span class='input-group-addon'><i class='fa fa-calendar'></i></span></div></div></div>")
                        .append("<div class='col-md-3'><div class='form-group'><label class='control-label'>End Time</label><div class='input-group input-append bootstrap-timepicker'><input type='text' id='endTime' name='endTime' value='" + eTime + "' class='form-control time-picker'><span class='input-group-addon add-on'><i class='fa fa-clock-o'></i></span></div></div></div>")
                        .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Note</label><textarea class='form-control' id='form-field-22' name='note'>" + note + "</textarea></div></div>")
                        .append("<div class='col-md-6'><div class='form-group'><label class='checkbox-inline'><input type='checkbox' id='isAllDay' name='isAllDay' " + checked + " class='grey'>All Day</label></div></div>");

                        $modal.append("<script type='text/javascript' src='../Content/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'></script>");
                        $modal.append("<script type='text/javascript' src='../Content/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'></script>");
                        $modal.append("<script type='text/javascript' src='../Content/assets/plugins/select2/select2.min.js'></script>");
                        $modal.append("<script type='text/javascript' src='../Content/assets/js/form-elements.js'></script>");
                        $modal.append("<script type='text/javascript'>jQuery(document).ready(function () {  Main.init(); FormElements.init(); if ($('input.grey').is(':checked')) { $('#startTime').attr('disabled', true); $('#endTime').attr('disabled', true); }       $('input').on('ifChecked', function () { $('#startTime').attr('disabled', true); $('#startTime').val('12:00 AM'); $('#endTime').attr('disabled', true);  $('#endTime').val('12:00 AM'); });    $('input').on('ifUnchecked', function () { $('#startTime').attr('disabled', false); $('#startTime').val(startCurrentTime); $('#endTime').attr('disabled', false); $('#endTime').val(endCurrentTime); });  });  </script>");

                        $.ajax({
                            "url": "/Calendar/LoadDoctors/",
                            "type": "get",
                            "data": "&SelectedDoctorID=" + data.DoctorID,
                            "dataType": "json",
                            "success": function (data) {
                                $("#form-field-select-1").empty();

                                $.each(data, function () {
                                    if (this.Selected) {
                                        $("#form-field-select-1").append($("<option selected />").val(this.Value).text(this.Text)).find("select[name='categoryD']");
                                        $("#form-field-select-1").select2("val", this.Value);
                                    }
                                    else {
                                        $("#form-field-select-1").append($("<option />").val(this.Value).text(this.Text)).find("select[name='categoryD']");
                                    }
                                });
                            }
                        });

                        $.ajax({
                            "url": "/Calendar/LoadPatients/",
                            "type": "get",
                            "data": "&SelectedPatientID=" + data.PatientID,
                            "dataType": "json",
                            "success": function (data) {
                                $("#form-field-select-2").empty();

                                $.each(data, function () {
                                    if (this.Selected) {
                                        $("#form-field-select-2").append($("<option selected/>").val(this.Value).text(this.Text)).find("select[name='categoryP']");
                                        $("#form-field-select-2").select2("val", this.Value);
                                    }
                                    else {
                                        $("#form-field-select-2").append($("<option />").val(this.Value).text(this.Text)).find("select[name='categoryP']");
                                    }
                                });
                            }
                        });

                    }
                });

                $modal.modal({
                    backdrop: 'static'
                });
                $modal.find('.remove-event').show().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.remove-event').unbind('click').click(function () {

                    //Delete Appointment Confirmation
                    $modalConfirm.modal({
                        backdrop: 'static'
                    });

                    form = $("<form></form>");
                    form.append("<div class='row'></div>")
                     .append("<p>Are you sure you want to delete appointment <b>" + calEvent.title + "</b> ?</p>");

                    $modalConfirm.find('.modal-body').empty().prepend(form).end().find('.btn-primary').unbind('click').click(function () {
                        calendar.fullCalendar('removeEvents', function (ev) {
                            $.ajax({
                                type: "POST",
                                url: "/Calendar/DeleteAppointment",
                                data: "&AppointmentID=" + calEvent._id
                            });
                            return (ev._id == calEvent._id);
                        });
                        $modal.modal('hide');
                    });
                });

                $modal.find('.remove-event').show().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').click(function () {
                    form.submit();
                });

                $modal.find('form').on('submit', function () {
                    title = form.find("input[name='title']").val();
                    note = form.find("textarea[name='note']").val();
                    $patientID = form.find("select[name='categoryP'] option:checked").val();
                    $doctorID = form.find("select[name='categoryD'] option:checked").val();
                    isAllDay = form.find("input[name='isAllDay']").is(':checked');
                    startDate = form.find("input[name='startDate']").val();
                    startTime = form.find("input[name='startTime']").val();
                    endDate = form.find("input[name='endDate']").val();
                    endTime = form.find("input[name='endTime']").val();

                    allDay = isAllDay;
                    start = new Date(startDate + " " + startTime);
                    end = new Date(endDate + " " + endTime);

                    $categoryClass = form.find("select[name='category'] option:checked").val();
                    var clinicID = $('#clinic').val();

                    if (title !== null) {
                        $.ajax({
                            url: "/Calendar/EditAppointment",
                            type: "POST",
                            cache: false,
                            data: { appointmentID: calEvent.id, title: title, start: start, end: end, allDay: allDay, clinicID: clinicID, note: note, patientID: $patientID, doctorID: $doctorID },
                            error: function (xhr, status, error) {
                            },
                            success: function (data) {
                                calEvent.title = form.find("input[name='title']").val();
                                calEvent.start = start;
                                calEvent.allDay = allDay;
                                calEvent.end = end;
                                calendar.fullCalendar('updateEvent', calEvent);
                            }
                        });
                    }
                    $modal.modal('hide');
                    return false;
                });
            }
        });
    };
    return {
        init: function () {
            runCalendar();
        }
    };
} ();