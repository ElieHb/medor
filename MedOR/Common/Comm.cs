﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace MedOR.Common
{
    public class Comm
    {
        public class AuthorizeUserAttribute : AuthorizeAttribute
        {
            // Custom property
            public string AccessLevel { get; set; }

            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                var isAuthorized = base.AuthorizeCore(httpContext);
                if (!isAuthorized)
                {
                    return false;
                }

                string privilegeLevels = string.Join("", GetUserRights(httpContext.User.Identity.Name.ToString(), this.AccessLevel)); // Call another method to get rights of the user from DB

                if (privilegeLevels.Contains(this.AccessLevel))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private static String GetUserRights(String UserID, String accessLevel)
        {
            var userID = new Guid(UserID);
            var asset = accessLevel.Split(' ')[1];
            DataAccess.MedOREntities _MedOrDB = new DataAccess.MedOREntities();
            var permission = (from u in _MedOrDB.Users
                              where u.UserID == userID
                              from r in _MedOrDB.Roles
                              where r.RoleID == u.RoleID
                              from p in _MedOrDB.Permissions
                              where p.PermissionName == asset
                              from s in _MedOrDB.Securities
                              where s.RoleID == r.RoleID && s.PermissionID == p.PermissionID
                              select (s.View ? "View " + p.PermissionName : String.Empty) + "|"
                              + (s.Create ? "Create " + p.PermissionName : String.Empty) + "|"
                              + (s.Edit ? "Edit " + p.PermissionName : String.Empty) + "|"
                              + (s.Delete ? "Delete " + p.PermissionName : String.Empty)).FirstOrDefault();

            return permission;
        }

        public static List<SelectListItem> GetCountryList()
        {
            RegionInfo country = new RegionInfo(new CultureInfo("en-US", false).LCID);

            List<SelectListItem> list = new List<SelectListItem>();
            var cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            foreach (CultureInfo cul in cultures)
            {
                country = new RegionInfo(new CultureInfo(cul.Name, false).LCID);
                list.Add(new SelectListItem() { Text = country.DisplayName, Value = country.DisplayName});
            }

            return list.OrderBy(x => x.Text).ToList();
        }

        public static List<SelectListItem> GetDaysList()
        {
            int days = 32;
            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 1; i < days; i++)
            {
                list.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }

            return list.OrderBy(x => int.Parse(x.Text)).ToList();
        }

        public static List<SelectListItem> GetMonthsList()
        {
            int months = 13;
            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 1; i < months; i++) 
            {
                list.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }

            return list.OrderBy(x => int.Parse(x.Text)).ToList();
        }

        public static List<SelectListItem> GetClinicList(IEnumerable<DataModel.ClinicModel> clinics)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (clinics != null)
            {
                foreach (var clinic in clinics)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = clinic.ClinicID.ToString(),
                        Text = clinic.Name + ", "
                    });
                }
            }
            return list;
        }

        public static List<SelectListItem> GetDoctorList(IEnumerable<DataModel.DoctorModel> doctors)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (doctors != null)
            {
                foreach (var doctor in doctors)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = doctor.DoctorID.ToString(),
                        Text = doctor.FullName + ", "
                    });
                }
            }
            return list;
        }
        public static List<SelectListItem> GetPatientListDrop(IEnumerable<DataModel.PaitentModel> Patients)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (Patients != null)
            {
                foreach (var patient in Patients)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = patient.PatientID.ToString(),
                        Text = patient.FullName+ ", " 
                    });
                }
            }
            return list;
        }

        public static List<SelectListItem> GetRoleList(IEnumerable<DataModel.RoleModel> roles)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (roles != null)
            {
                foreach (var role in roles)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = role.RoleID.ToString(),
                        Text = role.Name
                    });
                }
            }
            return list;
        }

        public static List<SelectListItem> GetUserList(IEnumerable<DataModel.UserModel> users)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (users != null)
            {
                foreach (var user in users)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = user.UserID.ToString(),
                        Text = user.FirstName + " " + user.LastName
                    });
                }
            }
            return list;
        }

        public static List<SelectListItem> GetSpecialtyList(IEnumerable<DataModel.DropDownModel> specialties)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (specialties != null)
            {
                foreach (var specialty in specialties)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = specialty.DropDownID.ToString(),
                        Text = specialty.Value
                    });
                }
            }
            return list;
        }

        public static List<SelectListItem> GetAvailableDoctorList(IEnumerable<DataModel.DoctorModel> doctors, Guid? SelectedDoctorID)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (doctors != null)
            {
                foreach (var doctor in doctors)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = doctor.DoctorID.ToString(),
                        Text = doctor.FirstName + " " + doctor.LastName,
                        Selected = doctor.DoctorID == SelectedDoctorID ? true : false
                    });
                }
            }
            return list;
        }

        public static List<SelectListItem> GetPatientList(IEnumerable<DataModel.PaitentModel> patiets, Guid? SelectedPatientID) 
        {
             List<SelectListItem> list = new List<SelectListItem>();
            if (patiets != null)
            {
                foreach (var patient in patiets)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = patient.PatientID.ToString(),
                        Text = patient.FirstName + " " + patient.LastName,
                        Selected = patient.PatientID == SelectedPatientID ? true : false
                    });
                }
            }
            return list;
        }

        //Layout Styles********************

        public static SelectList GetLayoutList()
        {
            var currentUser = HttpContext.Current.User.Identity.Name;
            var UserID = new Guid(currentUser);
            DataAccess.MedOREntities _MedOrDB = new DataAccess.MedOREntities();
            var layout = (from l in _MedOrDB.LayoutStyles where l.UserID == UserID select l.Layout).FirstOrDefault();

            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Wide", Value = "default" });
            list.Add(new SelectListItem { Text = "Boxed", Value = "boxed" });

            SelectList selectList = new SelectList(list, "Value", "Text", layout);

            return selectList;
        }

        public static SelectList GetHeaderList()
        {
            var currentUser = HttpContext.Current.User.Identity.Name;
            var UserID = new Guid(currentUser);
            DataAccess.MedOREntities _MedOrDB = new DataAccess.MedOREntities();
            var header = (from l in _MedOrDB.LayoutStyles where l.UserID == UserID select l.Header).FirstOrDefault();

            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Fixed", Value = "fixed" });
            list.Add(new SelectListItem { Text = "Default", Value = "default" });

            SelectList selectList = new SelectList(list, "Value", "Text", header);

            return selectList;
        }

        public static SelectList GetFooterList()
        {
            var currentUser = HttpContext.Current.User.Identity.Name;
            var UserID = new Guid(currentUser);
            DataAccess.MedOREntities _MedOrDB = new DataAccess.MedOREntities();
            var footer = (from l in _MedOrDB.LayoutStyles where l.UserID == UserID select l.Footer).FirstOrDefault();

            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Default", Value = "default" });
            list.Add(new SelectListItem { Text = "Fixed", Value = "fixed" });

            SelectList selectList = new SelectList(list, "Value", "Text", footer);

            return selectList;
        }

        public static List<SelectListItem> GetBloodTypeList()
        {
            DataAccess.MedOREntities _MedOrDB = new DataAccess.MedOREntities();
            var bloodTypes = (from b in _MedOrDB.DropDowns where b.Type == "BloodType" select new { DropdownID = b.DropDownID, Type = b.Type, Value = b.Value}).ToList();
            List<SelectListItem> list = new List<SelectListItem>();

            if (bloodTypes != null)
            {
                foreach (var bloodType in bloodTypes)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = bloodType.DropdownID.ToString(),
                        Text = bloodType.Value,
                    });
                }
            }

            return list.OrderBy(o => o.Text).ToList();
        }
    }
}