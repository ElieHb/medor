﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataServices
{
    public class Service : BaseService
    {
        public Service()
        { }

        #region Login
        //using simple membership
        #endregion

        #region UserProfile
        public int GetUserProfileIDByUsername(String username) 
        {
            var user = _MedORDB.UserProfiles.Where(x => x.UserName == username).Select(x => x.UserId).FirstOrDefault();
            return user;
        }
        public String GetEmailByUsername(String username)
        {
            var email = _MedORDB.UserProfiles.Where(x => x.UserName == username).Select(x => x.EmailId).FirstOrDefault(); 
            return email;
        }
        public Int32 UpdateUserProfile(int userProfileID, String email)
        {
            var updateUserProfile = _MedORDB.UserProfiles.Where(x => x.UserId == userProfileID).FirstOrDefault();
            int success = 0;
            if (userProfileID != 0 )
            {
                updateUserProfile.EmailId = email;
                success = _MedORDB.SaveChanges();
            }

            return success;
        }
        #endregion

        #region WebpagesMembership
        public Boolean CheckMatchUserIDAndToken(int userid, String rt)
        {
            var any = _MedORDB.webpages_Membership.Where(x => x.UserId == userid && x.PasswordVerificationToken == rt).Any();  //&& (j.PasswordVerificationTokenExpirationDate < DateTime.Now)
            return any;
        }
        #endregion

        #region User
        public DataModel.UserModel GetUserByUserProfileID(int userProfileID)
        {
            var user = (from u in _MedORDB.Users
                        where u.UserProfileID == userProfileID
                        select new DataModel.UserModel
                        {
                            UserID = u.UserID,
                            Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                            Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault()
                        }).FirstOrDefault();
            return user;
        }
        public int GetUserProfileIDByUserID(Guid userID)
        {
            var userProfileID = _MedORDB.Users.Where(x => x.UserID == userID).Select(x => x.UserProfileID).FirstOrDefault();
            return userProfileID;
        }
        public String GetUsernameByUserID(Guid userID)
        {
            var username = (from u in _MedORDB.Users
                            where u.UserID == userID
                            from p in _MedORDB.UserProfiles
                            where p.UserId == u.UserProfileID
                            select p.UserName).FirstOrDefault();
            return username;
        }
        public IEnumerable<DataModel.UserModel> GetAllUsers()
        {

            var userModel = (from u in _MedORDB.Users
                             from r in _MedORDB.Roles
                             where r.RoleID == u.RoleID
                             select new DataModel.UserModel
                             {
                                 UserID = u.UserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Gender = u.Gender,
                                 Phone = u.Phone,
                                 Address = u.Address,
                                 City = u.City,
                                 Country = u.Country,
                                 Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                                 Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault(),
                                 Image = u.Image,
                                 RoleID = u.RoleID,
                                 RoleName = r.Name,
                                 CreatedBy = u.CreatedBy,
                                 CreatedByName = (from m in _MedORDB.Users where m.UserID == u.CreatedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 CreatedOn = u.CreatedOn,
                                 ModifiedBy = u.ModifiedBy,
                                 ModifiedByName = (from m in _MedORDB.Users where m.UserID == u.ModifiedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 ModifiedOn = u.ModifiedOn,
                                 SelectedClinicIDs = from us in _MedORDB.UserClinics
                                                     where us.UserID == u.UserID
                                                     from c in _MedORDB.Clinics
                                                     where us.ClinicID == c.ClinicID
                                                     select c.ClinicID,
                                 SelectedDoctorIDs = from us in _MedORDB.UserDoctors
                                                     where us.UserID == u.UserID
                                                     from d in _MedORDB.Doctors
                                                     where us.DoctorID == d.DoctorID
                                                     select d.DoctorID,
                                 SelectedClinicNames = from us in _MedORDB.UserClinics
                                                       where us.UserID == u.UserID
                                                       from c in _MedORDB.Clinics
                                                       where us.ClinicID == c.ClinicID
                                                       select c.Name + ", ",
                                 SelectedDoctorNames = from us in _MedORDB.UserDoctors
                                                       where us.UserID == u.UserID
                                                       from d in _MedORDB.Doctors
                                                       where us.DoctorID == d.DoctorID
                                                       select d.FirstName + " " + d.LastName + ", ",
                                 SecurityModel = (from p in _MedORDB.Permissions
                                                  where p.PermissionName == "Users"
                                                  from s in _MedORDB.Securities
                                                  where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                  select new DataModel.SecurityModel
                                                      {
                                                          View = s.View,
                                                          Create = s.Create,
                                                          Edit = s.Edit,
                                                          Delete = s.Delete
                                                      }).FirstOrDefault()
                             }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName);
            return userModel;
        }
        public DataModel.UserModel GetAllAccess()
        {
            var userModel = new DataModel.UserModel
                            {
                                DoctorModels = (from d in _MedORDB.Doctors select new DataModel.DoctorModel { FullName = d.FirstName + " " + d.LastName, DoctorID = d.DoctorID }).OrderBy(o => o.FullName),
                                ClinicModels = (from c in _MedORDB.Clinics select new DataModel.ClinicModel { Name = c.Name, ClinicID = c.ClinicID }).OrderBy(o => o.Name),
                                RoleModels = (from r in _MedORDB.Roles select new DataModel.RoleModel { Name = r.Name, RoleID = r.RoleID }).OrderBy(o => o.Name),
                                PaitentModels = (from t in _MedORDB.Patients select new DataModel.PaitentModel { FullName = t.FirstName + " " + t.LastName,PatientID = t.PatientID }).OrderBy(o => o.FullName),

            };
            return userModel;
        }
        public DataModel.UserModel GetUserByID(Guid UserID)
        {
            var userModel = (from u in _MedORDB.Users
                             where u.UserID == UserID
                             select new DataModel.UserModel
                             {
                                 UserID = u.UserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Gender = u.Gender,
                                 Phone = u.Phone,
                                 Address = u.Address,
                                 City = u.City,
                                 Country = u.Country,
                                 Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                                 Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault(),
                                 Role = u.RoleID,
                                 RoleID = u.RoleID,
                                 RoleName = (from r in _MedORDB.Roles
                                             where r.RoleID == u.RoleID
                                             select r.Name).FirstOrDefault(),
                                 //CreatedBy = u.CreatedBy,
                                 //CreatedByName = (from m in _MedORDB.Users where m.UserID == u.CreatedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 //CreatedOn = u.CreatedOn,
                                 //ModifiedBy = u.ModifiedBy,
                                 //ModifiedByName = (from m in _MedORDB.Users where m.UserID == u.ModifiedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 //ModifiedOn = u.ModifiedOn,
                                 SelectedClinicIDs = from us in _MedORDB.UserClinics
                                                     where us.UserID == u.UserID
                                                     from c in _MedORDB.Clinics
                                                     where us.ClinicID == c.ClinicID
                                                     select c.ClinicID,
                                 SelectedDoctorIDs = from us in _MedORDB.UserDoctors
                                                     where us.UserID == u.UserID
                                                     from d in _MedORDB.Doctors
                                                     where us.DoctorID == d.DoctorID
                                                     select d.DoctorID,
                                 SelectedClinicNames = from us in _MedORDB.UserClinics
                                                       where us.UserID == u.UserID
                                                       from c in _MedORDB.Clinics
                                                       where us.ClinicID == c.ClinicID
                                                       select c.Name + ", ",
                                 SelectedDoctorNames = from us in _MedORDB.UserDoctors
                                                       where us.UserID == u.UserID
                                                       from d in _MedORDB.Doctors
                                                       where us.DoctorID == d.DoctorID
                                                       select d.FirstName + " " + d.LastName + ", ",
                                 LastLoggedIn = u.LastLoggedIn,
                                 Image = u.Image
                             }).FirstOrDefault();

            userModel.LoggedInSince = userModel.LastLoggedIn.HasValue ? TimeAgo(userModel.LastLoggedIn.Value) : "never";

            return userModel;
        }

        public static string TimeAgo(DateTime date)
        {
            TimeSpan timeSince = DateTime.Now.Subtract(date);
            if (timeSince.TotalMilliseconds < 1) return "not yet";
            if (timeSince.TotalMinutes < 1) return "just now";
            if (timeSince.TotalMinutes < 2) return "1 minute ago";
            if (timeSince.TotalMinutes < 60) return string.Format("{0} minutes ago", timeSince.Minutes);
            if (timeSince.TotalMinutes < 120) return "1 hour ago";
            if (timeSince.TotalHours < 24) return string.Format("{0} hours ago", timeSince.Hours);
            if (timeSince.TotalDays < 2) return "yesterday";
            if (timeSince.TotalDays < 7) return string.Format("{0} days ago", timeSince.Days);
            if (timeSince.TotalDays < 14) return "last week";
            if (timeSince.TotalDays < 21) return "2 weeks ago";
            if (timeSince.TotalDays < 28) return "3 weeks ago";
            if (timeSince.TotalDays < 60) return "last month";
            if (timeSince.TotalDays < 365) return string.Format("{0} months ago", Math.Round(timeSince.TotalDays / 30));
            if (timeSince.TotalDays < 730) return "last year"; //last but not least...
            return string.Format("{0} years ago", Math.Round(timeSince.TotalDays / 365));
        }

        public Byte[] GetUserImage(Guid UserID)
        {
            var userImage = (from u in _MedORDB.Users
                             where u.UserID == UserID
                             select u.Image).FirstOrDefault();
            return userImage;
        }

        public DataModel.UserModel GetUserByIDForLockScreen(Guid UserID)
        {
            var userModel = (from u in _MedORDB.Users
                             where u.UserID == UserID
                             select new DataModel.UserModel
                             {
                                 UserID = u.UserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                                 Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault(),
                             }).FirstOrDefault();
            return userModel;
        }
        public Int32 AddNewUser(DataModel.UserModel model)
        {
            DataAccess.User newUser = new DataAccess.User();
            newUser.UserID = Guid.NewGuid();
            newUser.UserProfileID = model.UserProfileID;
            newUser.RoleID = model.Role;
            newUser.FirstName = model.FirstName;
            newUser.LastName = model.LastName;
            newUser.Gender = model.Gender;
            newUser.Phone = model.Phone;
            newUser.Address = model.Address;
            newUser.City = model.City;
            newUser.Country = model.Country;
            newUser.Image = model.Image;
            newUser.CreatedBy = model.CreatedBy;
            newUser.CreatedOn = model.CreatedOn;
            _MedORDB.Users.AddObject(newUser);
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                if (model.SelectedClinicIDs != null)
                {
                    foreach (var clinicID in model.SelectedClinicIDs)
                    {
                        AddNewUserClinic(newUser.UserID, clinicID, model.CreatedBy, model.CreatedOn);
                    }
                }

                if (model.SelectedDoctorIDs != null)
                {
                    foreach (var doctorID in model.SelectedDoctorIDs)
                    {
                        AddNewUserDoctor(newUser.UserID, doctorID, model.CreatedBy, model.CreatedOn);
                    }
                }
            }

            return success;
        }
        public Int32 UpdateUser(DataModel.UserModel model)
        {
            var updateUser = _MedORDB.Users.Where(x => x.UserID == model.UserID).FirstOrDefault();
            updateUser.RoleID = model.Role;
            updateUser.FirstName = model.FirstName;
            updateUser.LastName = model.LastName;
            updateUser.Gender = model.Gender;
            updateUser.Phone = model.Phone;
            updateUser.Address = model.Address;
            updateUser.City = model.City;
            updateUser.Country = model.Country;
            updateUser.Image = model.Image;
            updateUser.ModifiedBy = model.ModifiedBy;
            updateUser.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                //Clinics access
                success = UpdateUserClinic(model.UserID, model.SelectedClinicIDs, model.ModifiedBy.Value, model.ModifiedOn.Value);

                //Doctors access
                success = UpdateUserDoctor(model.UserID, model.SelectedDoctorIDs, model.ModifiedBy.Value, model.ModifiedOn.Value);
            }

            return success;
        }
        public Int32 UpdateUserLastLoggedIn(DataModel.UserModel model)
        {
            var updateUser = _MedORDB.Users.Where(x => x.UserID == model.UserID).FirstOrDefault();
            updateUser.LastLoggedIn = model.LastLoggedIn;
            updateUser.ModifiedBy = model.ModifiedBy;
            updateUser.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();

            return success;
        }
        public Int32 DeleteUserByID(Guid UserID)
        {
            var user = _MedORDB.Users.Where(x => x.UserID == UserID).FirstOrDefault();
            int success = 0;
            if (user != null)
            {
                //Delete all userClinics related to this user
                var userClinics = _MedORDB.UserClinics.Where(x => x.UserID == UserID);
                foreach(var userClinic in userClinics)
                    _MedORDB.UserClinics.DeleteObject(userClinic);

                //Delete all userDoctors related to this user
                var userDoctors = _MedORDB.UserDoctors.Where(x => x.UserID == UserID);
                foreach (var userDoctor in userDoctors)
                    _MedORDB.UserDoctors.DeleteObject(userDoctor);

                _MedORDB.Users.DeleteObject(user);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion

        #region Invoices
        public DataModel.InvoicesModel GetInvoicesByID(Guid CreatedBy)
        {
            var InvoicesModel = (from pi in _MedORDB.Invoices
                                 where pi.CreatedBy == CreatedBy
                                 select new DataModel.InvoicesModel
                                 {
                                     InvoiceID = pi.InvoiceID,
                                     PatientID = pi.PatientID,
                                     DoctorID = pi.DoctorID,
                                     Total = pi.Total,
                                     VAT = (int)pi.VAT,
                                     SubTotal = pi.SubTotal,
                                     CreatedON = pi.CreatedON,
                                     CreatedBy = pi.CreatedBy,
                                     ModifiedON = pi.ModifiedOn,
                                     ModifiedBy = pi.ModifiedBy

                                 }).FirstOrDefault();
            return InvoicesModel;
        }
        #endregion

        #region InvoiceItem
        public DataModel.InvoiceItemModel GetInvoicesModelByInvoice(Guid InvoiceID)
        {
            var InvoiceItemModel = (from pi in _MedORDB.InvoiceItems
                                     where pi.InvoiceID == InvoiceID
                                     select new DataModel.InvoiceItemModel
                                     {
                                         InvoiceItemID = pi.InvoiceItemID,
                                         InvoiceID = pi.InvoiceID,
                                         InventoryID = pi.InventoryID,
                                         ConsultationID = pi.ConsultationID,
                                         Description = pi.Description,
                                         UnitCost = pi.Unicost,
                                         Total = pi.Total,
                                         Discount = (int)pi.Discount,
                                         Quantity = pi.Quantity




                                     }).FirstOrDefault();
            return InvoiceItemModel;
        }

        public Int32 AddNewItemInvoice(DataModel.UserModel model)
        {
            DataAccess.InvoiceItem newItem = new DataAccess.InvoiceItem();
            newItem.InvoiceItemID = Guid.NewGuid();
            newItem.InvoiceID= Guid.NewGuid();
            newItem.InventoryID = Guid.NewGuid();
            newItem.ConsultationID = Guid.NewGuid();
            newItem.Description = model.SelectedDescription;
            newItem.Quantity = model.SelectedQuantity;
            newItem.Unicost = model.SelectedUnitCost;
            newItem.Total = model.SelectedTotal;
            newItem.Discount = model.Selec;
            _MedORDB.InvoiceItems.AddObject(newItem);
            var success = _MedORDB.SaveChanges();

            //if (success != 0)
            //{
          // AddNewItemInvoice(model.CreatedBy, newClinic.ClinicID, model.CreatedBy, model.CreatedOn);
            //}
            return success;
        }
        #endregion

        #region UserClinic
        public Int32 AddNewUserClinic(Guid userID, Guid clinicID, Guid createdBy, DateTime createdOn)
        {
            DataAccess.UserClinic newUserClinic = new DataAccess.UserClinic();
            newUserClinic.UserClinicID = Guid.NewGuid();
            newUserClinic.UserID = userID;
            newUserClinic.ClinicID = clinicID;
            newUserClinic.CreatedBy = createdBy;
            newUserClinic.CreatedOn = createdOn;
            _MedORDB.UserClinics.AddObject(newUserClinic);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateUserClinic(Guid userID, IEnumerable<Guid> selectedClinicIDs, Guid createdBy, DateTime createdOn)
        {
            var success = 1;
            var previousClinicIDs = _MedORDB.UserClinics.Where(x => x.UserID == userID).Select(x => x.ClinicID);
            var previousUserClinics = _MedORDB.UserClinics.Where(x => x.UserID == userID);
            IQueryable<Guid> removeClinicIDs;

            if(selectedClinicIDs != null)
                removeClinicIDs = previousClinicIDs.Except(selectedClinicIDs);
            else
                removeClinicIDs = previousClinicIDs;

               if (removeClinicIDs.Count() != 0)
               {
                   foreach (var removeClinicID in removeClinicIDs)
                   {
                       _MedORDB.UserClinics.DeleteObject(previousUserClinics.Where(x => x.ClinicID == removeClinicID).FirstOrDefault());
                   }
                   success = _MedORDB.SaveChanges();
               }               

            if (selectedClinicIDs != null)
            {
                var addClinicIDs = selectedClinicIDs.Except(previousClinicIDs);
                foreach (var addClinicID in addClinicIDs)
                    success = AddNewUserClinic(userID, addClinicID, createdBy, createdOn);
            }

            return success;
        }
        #endregion

        #region UserDoctor
        public Int32 AddNewUserDoctor(Guid userID, Guid doctorID, Guid createdBy, DateTime createdOn)
        {
            DataAccess.UserDoctor newUserDoctor = new DataAccess.UserDoctor();
            newUserDoctor.UserDoctorID = Guid.NewGuid();
            newUserDoctor.UserID = userID;
            newUserDoctor.DoctorID = doctorID;
            newUserDoctor.CreatedBy = createdBy;
            newUserDoctor.CreatedOn = createdOn;
            _MedORDB.UserDoctors.AddObject(newUserDoctor);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateUserDoctor(Guid userID, IEnumerable<Guid> selectedDoctorIDs, Guid createdBy, DateTime createdOn)
        {
            var success = 1;
            var previousDoctorIDs = _MedORDB.UserDoctors.Where(x => x.UserID == userID).Select(x => x.DoctorID);
            var previousUserDoctors = _MedORDB.UserDoctors.Where(x => x.UserID == userID);
            IQueryable<Guid> removeDoctorIDs;

            if (selectedDoctorIDs != null)
                removeDoctorIDs = previousDoctorIDs.Except(selectedDoctorIDs);
            else
                removeDoctorIDs = previousDoctorIDs;

            if (removeDoctorIDs.Count() != 0)
            {
                foreach (var removeDoctorID in removeDoctorIDs)
                {
                    _MedORDB.UserDoctors.DeleteObject(previousUserDoctors.Where(x => x.DoctorID == removeDoctorID).FirstOrDefault());
                }
                success = _MedORDB.SaveChanges();
            }

            if (selectedDoctorIDs != null)
            {
                var addDoctorIDs = selectedDoctorIDs.Except(previousDoctorIDs);
                foreach (var addDoctorID in addDoctorIDs)
                    success = AddNewUserDoctor(userID, addDoctorID, createdBy, createdOn);
            }

            return success;
        }
        #endregion

        #region Clinic
        public IEnumerable<DataModel.ClinicModel> GetClinicsByUserID(Guid UserID)
        {
            var clinicModel = (from uc in _MedORDB.UserClinics
                               where uc.UserID == UserID
                               from c in _MedORDB.Clinics
                               where c.ClinicID == uc.ClinicID
                               select new DataModel.ClinicModel
                               {
                                   ClinicID = c.ClinicID,
                                   Name = c.Name,
                                   Address = c.Address,
                                   City = c.City,
                                   Country = c.Country,
                                   Phone = c.Phone,
                                   CreatedBy = c.CreatedBy,
                                   CreatedByName = (from u in _MedORDB.Users where u.UserID == c.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   CreatedOn = c.CreatedOn,
                                   ModifiedBy = c.ModifiedBy,
                                   ModifiedByName = (from u in _MedORDB.Users where u.UserID == c.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   ModifiedOn = c.ModifiedOn,
                                   SecurityModel = (from u in _MedORDB.Users
                                                    where u.UserID == UserID
                                                    from r in _MedORDB.Roles
                                                    where r.RoleID == u.RoleID
                                                    from p in _MedORDB.Permissions
                                                    where p.PermissionName == "Clinics"
                                                    from s in _MedORDB.Securities
                                                    where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                    select new DataModel.SecurityModel
                                                    {
                                                        View = s.View,
                                                        Create = s.Create,
                                                        Edit = s.Edit,
                                                        Delete = s.Delete
                                                    }).FirstOrDefault(),
                               }).OrderBy(o => o.Name);
            return clinicModel;
        }
        public Int32 AddNewClinic(DataModel.ClinicModel model)
        {
            DataAccess.Clinic newClinic = new DataAccess.Clinic();

            newClinic.ClinicID = Guid.NewGuid();
            newClinic.Name = model.Name;
            newClinic.Address = model.Address;
            newClinic.City = model.City;
            newClinic.Country = model.Country;
            newClinic.Phone = model.Phone;
            newClinic.CreatedBy = model.CreatedBy;
            newClinic.CreatedOn = model.CreatedOn;
            _MedORDB.Clinics.AddObject(newClinic);
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                AddNewUserClinic(model.CreatedBy, newClinic.ClinicID, model.CreatedBy, model.CreatedOn);
            }
            return success;
        }
        public DataModel.ClinicModel GetClinicByID(Guid ClinicID)
        {
            var clinicModel = (from c in _MedORDB.Clinics
                               where c.ClinicID == ClinicID
                               select new DataModel.ClinicModel
                               {
                                   ClinicID = c.ClinicID,
                                   Name = c.Name,
                                   Address = c.Address,
                                   City = c.City,
                                   Country = c.Country,
                                   Phone = c.Phone,
                               }).FirstOrDefault();
            return clinicModel;
        }
        public Int32 UpdateClinic(DataModel.ClinicModel model)
        {
            var editClinic = _MedORDB.Clinics.Where(x => x.ClinicID == model.ClinicID).FirstOrDefault();
            editClinic.Name = model.Name;
            editClinic.Address = model.Address;
            editClinic.City = model.City;
            editClinic.Country = model.Country;
            editClinic.Phone = model.Phone;
            editClinic.ModifiedBy = model.ModifiedBy;
            editClinic.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeleteClinicByID(Guid ClinicID)
        {
            var clinic = _MedORDB.Clinics.Where(x => x.ClinicID == ClinicID).FirstOrDefault();
            int success = 0;
            if (clinic != null)
            {
                _MedORDB.Clinics.DeleteObject(clinic);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion

        #region Security
        public DataModel.RoleModel GetAllPermissionsAndUsersForRoleModel()
        {
            var roleModel = new DataModel.RoleModel
                            {
                                PermissionModels = (from p in _MedORDB.Permissions
                                                    select new DataModel.PermissionModel
                                                    {
                                                        PermissionID = p.PermissionID,
                                                        PermissionName = p.PermissionName
                                                    }).OrderBy(o => o.PermissionName).ToList(),
                                UserModels = (from u in _MedORDB.Users
                                              where u.RoleID == null 
                                              select new DataModel.UserModel
                                              {
                                                  UserID = u.UserID,
                                                  FirstName = u.FirstName,
                                                  LastName = u.LastName
                                              }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName),
                            };
            return roleModel;
        }
        public IEnumerable<DataModel.RoleModel> GetRoleAndPermissions(Guid UserID)
        {
            var RoleModels = (from r in _MedORDB.Roles
                              select new DataModel.RoleModel
                              {
                                  RoleID = r.RoleID,
                                  Name = r.Name,
                                  CreatedBy = r.CreatedBy,
                                  CreatedByName = (from us in _MedORDB.Users where us.UserID == r.CreatedBy select us.FirstName + " " + us.LastName).FirstOrDefault(),
                                  CreatedOn = r.CreatedOn,
                                  ModifiedBy = r.ModifiedBy,
                                  ModifiedByName = (from us in _MedORDB.Users where us.UserID == r.ModifiedBy select us.FirstName + " " + us.LastName).FirstOrDefault(),
                                  ModifiedOn = r.ModifiedOn,
                                  SecurityModel = (from u in _MedORDB.Users
                                                   where u.UserID == UserID
                                                   from l in _MedORDB.Roles
                                                   where l.RoleID == u.RoleID
                                                   from p in _MedORDB.Permissions
                                                   where p.PermissionName == "Roles"
                                                   from s in _MedORDB.Securities
                                                   where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                   select new DataModel.SecurityModel
                                                    {
                                                        View = s.View,
                                                        Create = s.Create,
                                                        Edit = s.Edit,
                                                        Delete = s.Delete
                                                    }).FirstOrDefault()
                              }).OrderBy(o => o.Name);
            return RoleModels;
        }
        public List<DataModel.PermissionModel> GetAllPermissionsForRoleModel(Guid roleID)
        {
            var permissionModels = (from p in _MedORDB.Permissions
                                    select new DataModel.PermissionModel
                                    {
                                        PermissionID = p.PermissionID,
                                        PermissionName = p.PermissionName,
                                        View = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.View).FirstOrDefault(),
                                        Create = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.Create).FirstOrDefault(),
                                        Edit = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.Edit).FirstOrDefault(),
                                        Delete = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.Delete).FirstOrDefault(),
                                    }).OrderBy(o => o.PermissionName).ToList();
            return permissionModels;
        }
        public DataModel.RoleModel GetRoleByID(Guid RoleID)
        {
            var roleModel = (from r in _MedORDB.Roles
                             where r.RoleID == RoleID
                             select new DataModel.RoleModel
                             {
                                 RoleID = r.RoleID,
                                 Name = r.Name,
                                 SelectedUserIDs = from e in _MedORDB.Users
                                                   where e.RoleID == r.RoleID
                                                   select e.UserID,
                                 UserModels = (from u in _MedORDB.Users
                                               select new DataModel.UserModel
                                               {
                                                   UserID = u.UserID,
                                                   FirstName = u.FirstName,
                                                   LastName = u.LastName
                                               }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName),
                             }).FirstOrDefault();
            return roleModel;
        }

        public Int32 AddNewSecurityRole(DataModel.RoleModel model)
        {
            var success = 0;

            DataAccess.Role newRole = new DataAccess.Role();
            newRole.RoleID = Guid.NewGuid();
            newRole.Name = model.Name;
            newRole.CreatedOn = model.CreatedOn;
            newRole.CreatedBy = model.CreatedBy;
            _MedORDB.Roles.AddObject(newRole);
            success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                foreach (var permission in model.PermissionModels)
                {
                    DataAccess.Security newSecurity = new DataAccess.Security();
                    newSecurity.SecurityID = Guid.NewGuid();
                    newSecurity.RoleID = newRole.RoleID;
                    newSecurity.PermissionID = permission.PermissionID;
                    newSecurity.View = permission.View;
                    newSecurity.Create = permission.Create;
                    newSecurity.Edit = permission.Edit;
                    newSecurity.Delete = permission.Delete;
                    newSecurity.CreatedOn = model.CreatedOn;
                    newSecurity.CreatedBy = model.CreatedBy;
                    _MedORDB.Securities.AddObject(newSecurity);
                }
                success = _MedORDB.SaveChanges();
            } 
            
            if (success != 0)
            {
                if (model.SelectedUserIDs != null)
                {
                    foreach (var selectedUserID in model.SelectedUserIDs)
                    {
                        var updateUser = _MedORDB.Users.Where(x => x.UserID == selectedUserID).FirstOrDefault();
                        updateUser.RoleID = newRole.RoleID;
                        updateUser.ModifiedBy = model.CreatedBy;
                        updateUser.ModifiedOn = model.CreatedOn;
                    }
                    success = _MedORDB.SaveChanges();
                }
            }
            return success;
        }
        public Int32 UpdateSecurityRole(DataModel.RoleModel model)
        {
            var success = 1;

            var updateRole = _MedORDB.Roles.Where(x => x.RoleID == model.RoleID).FirstOrDefault();
            updateRole.Name = model.Name;
            updateRole.ModifiedOn = model.ModifiedOn;
            updateRole.ModifiedBy = model.ModifiedBy;
            success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                foreach (var permission in model.PermissionModels)
                {
                    var updateSecurity = _MedORDB.Securities.Where(x => x.RoleID == model.RoleID && x.PermissionID == permission.PermissionID).FirstOrDefault();
                    if (updateSecurity != null)
                    {
                        updateSecurity.View = permission.View;
                        updateSecurity.Create = permission.Create;
                        updateSecurity.Edit = permission.Edit;
                        updateSecurity.Delete = permission.Delete;
                        updateSecurity.ModifiedOn = model.ModifiedOn;
                        updateSecurity.ModifiedBy = model.ModifiedBy;
                    }
                    else
                    {
                        DataAccess.Security newSecurity = new DataAccess.Security();
                        newSecurity.SecurityID = Guid.NewGuid();
                        newSecurity.RoleID = model.RoleID;
                        newSecurity.PermissionID = permission.PermissionID;
                        newSecurity.View = permission.View;
                        newSecurity.Create = permission.Create;
                        newSecurity.Edit = permission.Edit;
                        newSecurity.Delete = permission.Delete;
                        newSecurity.CreatedOn = model.ModifiedOn.Value;
                        newSecurity.CreatedBy = model.ModifiedBy.Value;
                        _MedORDB.Securities.AddObject(newSecurity);
                    }
                    success = _MedORDB.SaveChanges();
                } 
            }

            if (success != 0)
            {
                var previousUsersID = _MedORDB.Users.Where(x => x.RoleID == model.RoleID).Select(u => u.UserID);
                var removeUsersID = previousUsersID.Except(model.SelectedUserIDs);

                foreach (var removeUserID in removeUsersID)
                {
                    var updateUser = _MedORDB.Users.Where(x => x.UserID == removeUserID).FirstOrDefault();
                    updateUser.RoleID = Guid.Empty;
                    updateUser.ModifiedBy = model.ModifiedBy;
                    updateUser.ModifiedOn = model.ModifiedOn;
                }

                foreach (var selectedUserID in model.SelectedUserIDs) 
                {
                    var updateUser = _MedORDB.Users.Where(x => x.UserID == selectedUserID).FirstOrDefault();
                    updateUser.RoleID = model.RoleID;
                    updateUser.ModifiedBy = model.ModifiedBy;
                    updateUser.ModifiedOn = model.ModifiedOn;
                }
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        public Int32 DeleteRoleByID(Guid RoleID)
        {
            var role = _MedORDB.Roles.Where(x => x.RoleID == RoleID).FirstOrDefault();
            int success = 0;
            if (role != null)
            {
                _MedORDB.Roles.DeleteObject(role);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion

        #region Doctor
        public IEnumerable<DataModel.DoctorModel> GetDoctorsByUserID(Guid UserID)
        {
            var doctorModel = (from ud in _MedORDB.UserDoctors
                               where ud.UserID == UserID
                               from d in _MedORDB.Doctors
                               where d.DoctorID == ud.DoctorID
                               select new DataModel.DoctorModel
                               {
                                   DoctorID = d.DoctorID,
                                   FirstName = d.FirstName,
                                   LastName = d.LastName,
                                   Gender = d.Gender,
                                   Email = d.Email,
                                   MobilePhone = d.MobilePhone,
                                   WorkPhone = d.WorkPhone,
                                   SpecialtyID = d.SpecialtyID,
                                   SpecialtyName = (from dd in _MedORDB.DropDowns where dd.DropDownID == d.SpecialtyID select dd.Value).FirstOrDefault(),
                                   CreatedBy = d.CreatedBy,
                                   CreatedByName = (from u in _MedORDB.Users where u.UserID == d.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   CreatedOn = d.CreatedOn,
                                   ModifiedBy = d.ModifiedBy,
                                   ModifiedByName = (from u in _MedORDB.Users where u.UserID == d.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   ModifiedOn = d.ModifiedOn,
                                   SecurityModel = (from u in _MedORDB.Users
                                                    where u.UserID == UserID
                                                    from r in _MedORDB.Roles
                                                    where r.RoleID == u.RoleID
                                                    from p in _MedORDB.Permissions
                                                    where p.PermissionName == "Doctors"
                                                    from s in _MedORDB.Securities
                                                    where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                    select new DataModel.SecurityModel
                                                    {
                                                        View = s.View,
                                                        Create = s.Create,
                                                        Edit = s.Edit,
                                                        Delete = s.Delete
                                                    }).FirstOrDefault()
                               }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName);
            return doctorModel;
        }
        public Int32 AddNewDoctor(DataModel.DoctorModel model)
        {
            DataAccess.Doctor newDoctor = new DataAccess.Doctor();
            newDoctor.DoctorID = Guid.NewGuid();
            newDoctor.FirstName = model.FirstName;
            newDoctor.LastName = model.LastName;
            newDoctor.Gender = model.Gender;
            newDoctor.Email = model.Email;
            newDoctor.MobilePhone = model.MobilePhone;
            newDoctor.WorkPhone = model.WorkPhone;
            newDoctor.SpecialtyID = model.Specialty;
            newDoctor.CreatedBy = model.CreatedBy;
            newDoctor.CreatedOn = model.CreatedOn;
            _MedORDB.Doctors.AddObject(newDoctor);
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                 AddNewUserDoctor(model.CreatedBy, newDoctor.DoctorID, model.CreatedBy, model.CreatedOn);
            }

            return success;
        }
        public DataModel.DoctorModel GetDoctorByID(Guid DoctorID)
        {
            var doctorModel = (from d in _MedORDB.Doctors
                               where d.DoctorID == DoctorID
                               select new DataModel.DoctorModel
                               {
                                   DoctorID = d.DoctorID,
                                   FirstName = d.FirstName,
                                   LastName = d.LastName,
                                   Gender = d.Gender,
                                   Email = d.Email,
                                   MobilePhone = d.MobilePhone,
                                   WorkPhone = d.WorkPhone,
                                   SpecialtyID = d.SpecialtyID,
                                   Specialty = d.SpecialtyID,
                                   DropDownModel = from dd in _MedORDB.DropDowns
                                                  where dd.Type == "Specialty"
                                                  select new DataModel.DropDownModel
                                                  {
                                                      DropDownID = dd.DropDownID,
                                                      Value = dd.Value
                                                  }
                               }).FirstOrDefault();
            return doctorModel;
        }
        public Int32 UpdateDoctor(DataModel.DoctorModel model)
        {
            var editDoctor = _MedORDB.Doctors.Where(x => x.DoctorID == model.DoctorID).FirstOrDefault();
            editDoctor.FirstName = model.FirstName;
            editDoctor.LastName = model.LastName;
            editDoctor.Gender = model.Gender;
            editDoctor.Email = model.Email;
            editDoctor.MobilePhone = model.MobilePhone;
            editDoctor.WorkPhone = model.WorkPhone;
            editDoctor.SpecialtyID = model.Specialty;
            editDoctor.ModifiedBy = model.ModifiedBy;
            editDoctor.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeleteDoctorByID(Guid DoctorID)
        {
            var doctor = _MedORDB.Doctors.Where(x => x.DoctorID == DoctorID).FirstOrDefault();
            int success = 0;
            if (doctor != null)
            {
                _MedORDB.Doctors.DeleteObject(doctor);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        public DataModel.DoctorModel GetSpecialties()
        {
            var doctorModel = new DataModel.DoctorModel
                              {
                                  DropDownModel = from dd in _MedORDB.DropDowns
                                                 where dd.Type == "Specialty"
                                                 select new DataModel.DropDownModel
                                                 {
                                                     DropDownID = dd.DropDownID,
                                                     Value = dd.Value
                                                 }
                              };
            return doctorModel;
        }
        #endregion  

        #region Patient
        public IEnumerable<DataModel.PaitentModel> GetAllPatients(Guid UserID)
        {
            var patientModel = (from p in _MedORDB.Patients
                                select new DataModel.PaitentModel
                                {
                                    PatientID = p.PatientID,
                                    FirstName = p.FirstName,
                                    MiddleName = p.MiddleName,
                                    LastName = p.LastName,
                                    PatientEmail = p.Email,
                                    City = p.City,
                                    Image = p.Image,
                                    MobilePhone = p.MobilePhone,
                                    HomePhone = p.HomePhone,
                                    CreatedBy = p.CreatedBy,
                                    CreatedByName = (from u in _MedORDB.Users where u.UserID == p.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                    CreatedOn = p.CreatedOn,
                                    ModifiedBy = p.ModifiedBy,
                                    ModifiedByName = (from u in _MedORDB.Users where u.UserID == p.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                    ModifiedOn = p.ModifiedOn,
                                    SecurityModel = (from u in _MedORDB.Users
                                                     where u.UserID == UserID
                                                     from r in _MedORDB.Roles
                                                     where r.RoleID == u.RoleID
                                                     //from m in _MedORDB.Permissions
                                                     //where m.PermissionName == "Patients"
                                                     //from s in _MedORDB.Securities
                                                     //where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID
                                                     select new DataModel.SecurityModel
                                                     {
                                                         View = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.View).FirstOrDefault(),
                                                         Create = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Create).FirstOrDefault(),
                                                         Edit = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Edit).FirstOrDefault(),
                                                         Delete = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Delete).FirstOrDefault(),
                                                         MedicalHistoryView = (from m in _MedORDB.Permissions where m.PermissionName == "MedicalHistory" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.View).FirstOrDefault(),
                                                     }).FirstOrDefault()
                                }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName);
            return patientModel;
        }
        public Int32 AddNewPatient(DataModel.PaitentModel model)
        {
            DataAccess.Patient newPatient = new DataAccess.Patient();
            newPatient.PatientID = Guid.NewGuid();
            newPatient.FirstName = model.FirstName;
            newPatient.MiddleName = model.MiddleName;
            newPatient.LastName = model.LastName;
            newPatient.DateOfBirth = model.DateOfBirth;
            newPatient.Gender = model.Gender;
            newPatient.Email = model.PatientEmail;
            newPatient.MobilePhone = model.MobilePhone;
            newPatient.HomePhone = model.HomePhone;
            newPatient.Address = model.Address;
            newPatient.City = model.City;
            newPatient.Image = model.Image;
            newPatient.Country = model.Country;
            newPatient.CreatedBy = model.CreatedBy;
            newPatient.CreatedOn = model.CreatedOn;
            _MedORDB.Patients.AddObject(newPatient);
            var success = _MedORDB.SaveChanges();

            return success;
        }
        public DataModel.PaitentModel GetPatientByID(Guid PatientID)
        {
            var patientModel = (from p in _MedORDB.Patients
                               where p.PatientID == PatientID
                               select new DataModel.PaitentModel
                               {
                                   PatientID = p.PatientID,
                                   FirstName = p.FirstName,
                                   MiddleName = p.MiddleName,
                                   LastName = p.LastName,
                                   Gender = p.Gender,
                                   PatientEmail = p.Email,
                                   MobilePhone = p.MobilePhone,
                                   HomePhone = p.HomePhone,
                                   DateOfBirth = p.DateOfBirth,
                                   Address = p.Address,
                                   Image = p.Image,
                                   City = p.City,
                                   Country = p.Country,
                               }).FirstOrDefault();
            return patientModel;
        }
        public Byte[] GetPatientImage(Guid PatientID)
        {
            var patientImage = (from p in _MedORDB.Patients
                                where p.PatientID == PatientID
                                select p.Image).FirstOrDefault();
            return patientImage;
        }
        public Int32 UpdatePatient(DataModel.PaitentModel model)
        {
            var editPatient = _MedORDB.Patients.Where(x => x.PatientID == model.PatientID).FirstOrDefault();
            editPatient.FirstName = model.FirstName;
            editPatient.MiddleName = model.MiddleName;
            editPatient.LastName = model.LastName;
            editPatient.Gender = model.Gender;
            editPatient.Email = model.PatientEmail;
            editPatient.MobilePhone = model.MobilePhone;
            editPatient.HomePhone = model.HomePhone;
            editPatient.DateOfBirth = model.DateOfBirth;
            editPatient.Address = model.Address;
            editPatient.City = model.City;
            editPatient.Country = model.Country;
            editPatient.Image = model.Image;
            editPatient.ModifiedBy = model.ModifiedBy;
            editPatient.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeletePatientByID(Guid PatientID)
        {
            var patient = _MedORDB.Patients.Where(x => x.PatientID == PatientID).FirstOrDefault();
            int success = 0;
            if (patient != null)
            {
                _MedORDB.Patients.DeleteObject(patient);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion
 
        #region Calendar
        public DataModel.CalendarModel GetCalendarByUserID(Guid UserID)
        {
            var calendarModel = new DataModel.CalendarModel
                                {
                                    ClinicModels = (from us in _MedORDB.UserClinics
                                                    where us.UserID == UserID
                                                    from c in _MedORDB.Clinics
                                                    where us.ClinicID == c.ClinicID
                                                    select new DataModel.ClinicModel
                                                    {
                                                        Name = c.Name,
                                                        ClinicID = c.ClinicID
                                                    }).OrderBy(o => o.Name)
                                };
            return calendarModel;
        }
        public IEnumerable<DataModel.AppointmentModel> GetAppointmentsByUserIDAndClinicID(Guid UserID, Guid ClinicID) 
        {
            var appointmentModel = (from uc in _MedORDB.UserClinics
                                    where uc.UserID == UserID && uc.ClinicID == ClinicID
                                    from a in _MedORDB.Appointments
                                    where a.ClinicID == uc.ClinicID
                                    select new DataModel.AppointmentModel
                                    {
                                        AppointmentID = a.AppointmentID,
                                        PatientID = a.PatientID,
                                        DoctorID = a.DoctorID,
                                        ClinicID = a.ClinicID,
                                        StartDateTime = a.StartDateTime.Value,
                                        EndDateTime = a.EndDateTime.Value,
                                        Title = a.Title,
                                        AllDay = a.AllDay,
                                        Note = a.Note,
                                        SecurityModel = (from u in _MedORDB.Users
                                                         where u.UserID == UserID
                                                         from r in _MedORDB.Roles
                                                         where r.RoleID == u.RoleID
                                                         from p in _MedORDB.Permissions
                                                         where p.PermissionName == "Appointments"
                                                         from s in _MedORDB.Securities
                                                         where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                         select new DataModel.SecurityModel
                                                         {
                                                             View = s.View,
                                                             Create = s.Create,
                                                             Edit = s.Edit,
                                                             Delete = s.Delete
                                                         }).FirstOrDefault()
                                    }).OrderBy(o => o.StartDateTime);
            return appointmentModel;
        }
        public DataModel.AppointmentModel GetAppointmentByID(Guid AppointmentID)
        {
            var appointmentModel = (from a in _MedORDB.Appointments
                                    where a.AppointmentID == AppointmentID
                                    select new DataModel.AppointmentModel
                                    {
                                        AppointmentID = a.AppointmentID,
                                        PatientID = a.PatientID,
                                        DoctorID = a.DoctorID,
                                        ClinicID = a.ClinicID,
                                        StartDateTime = a.StartDateTime,
                                        EndDateTime = a.EndDateTime,
                                        Title = a.Title,
                                        AllDay = a.AllDay,
                                        Note = a.Note,
                                    }).FirstOrDefault();
            return appointmentModel;
        }
        public Int32 AddNewAppointment(DataModel.AppointmentModel model)
        {
            DataAccess.Appointment newAppointment = new DataAccess.Appointment();
            newAppointment.AppointmentID = Guid.NewGuid();
            newAppointment.PatientID = model.PatientID;
            newAppointment.DoctorID = model.DoctorID;
            newAppointment.ClinicID = model.ClinicID;
            newAppointment.Title = model.title;
            newAppointment.Note = model.Note;
            newAppointment.AllDay = model.allDay;
            newAppointment.StartDateTime = model.StartDateTime;
            newAppointment.EndDateTime = model.EndDateTime;
            newAppointment.CreatedBy = model.CreatedBy;
            newAppointment.CreatedOn = model.CreatedOn;
            _MedORDB.Appointments.AddObject(newAppointment);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateAppointment(DataModel.AppointmentModel model)
        {
            var editAppointment = _MedORDB.Appointments.Where(x => x.AppointmentID == model.AppointmentID).FirstOrDefault();
            editAppointment.PatientID = model.PatientID;
            editAppointment.DoctorID = model.DoctorID;
            editAppointment.Title = model.title;
            editAppointment.Note = model.Note;
            editAppointment.StartDateTime = model.StartDateTime;
            editAppointment.EndDateTime = model.EndDateTime;
            editAppointment.AllDay = model.allDay;
            editAppointment.ModifiedBy = model.ModifiedBy;
            editAppointment.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateAppointmentDateTime(DataModel.AppointmentModel model)
        {
            var editAppointment = _MedORDB.Appointments.Where(x => x.AppointmentID == model.AppointmentID).FirstOrDefault();
            editAppointment.EndDateTime = model.EndDateTime;
            editAppointment.StartDateTime = model.StartDateTime;
            editAppointment.AllDay = model.AllDay;
            editAppointment.ModifiedBy = model.ModifiedBy;
            editAppointment.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateAppointmentEndDateTime(DataModel.AppointmentModel model)
        {
            var editAppointment = _MedORDB.Appointments.Where(x => x.AppointmentID == model.AppointmentID).FirstOrDefault();
            editAppointment.EndDateTime = model.EndDateTime;
            editAppointment.ModifiedBy = model.ModifiedBy;
            editAppointment.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeleteAppointmentByID(Guid AppointmentID)
        {
            var appointment = _MedORDB.Appointments.Where(x => x.AppointmentID == AppointmentID).FirstOrDefault();
            int success = 0;
            if (appointment != null)
            {
                _MedORDB.Appointments.DeleteObject(appointment);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion

        #region Layout
        public DataModel.LayoutStyleModel GetLayoutStyleByUserID(Guid UserID)
        {
            var layoutStyleModel = (from l in _MedORDB.LayoutStyles
                                    where l.UserID == UserID
                                    select new DataModel.LayoutStyleModel
                                    {
                                        LayoutStyleID = l.LayoutStyleID,
                                        Layout = l.Layout,
                                        Rtl = l.Rtl,
                                        Header = l.Header,
                                        Footer = l.Footer,
                                        Background = l.Background,
                                        PredefinedColorScheme = l.PredefinedColorScheme,
                                        Basic = l.Basic,
                                        Text = l.Text,
                                        Elements = l.Elements
                                    }).FirstOrDefault();
            return layoutStyleModel;
        }

        public Int32 UpdateLayoutStyle(DataModel.LayoutStyleModel model)
        {
            var editLayoutStyle = _MedORDB.LayoutStyles.Where(x => x.UserID == model.UserID).FirstOrDefault();
            if (editLayoutStyle != null)
            {
                editLayoutStyle.Rtl = model.Rtl;
                editLayoutStyle.Layout = model.Layout;
                editLayoutStyle.Header = model.Header;
                editLayoutStyle.Footer = model.Footer;
                editLayoutStyle.Background = model.Background;
                editLayoutStyle.PredefinedColorScheme = model.PredefinedColorScheme;
                editLayoutStyle.Basic = model.Basic;
                editLayoutStyle.Text = model.Text;
                editLayoutStyle.Elements = model.Elements;
            }
            else
            {
                DataAccess.LayoutStyle newLayoutStyleModel = new DataAccess.LayoutStyle();
                newLayoutStyleModel.LayoutStyleID = Guid.NewGuid();
                newLayoutStyleModel.UserID = model.UserID;
                newLayoutStyleModel.Rtl = model.Rtl;
                newLayoutStyleModel.Layout = model.Layout;
                newLayoutStyleModel.Header = model.Header;
                newLayoutStyleModel.Footer = model.Footer;
                newLayoutStyleModel.Background = model.Background;
                newLayoutStyleModel.PredefinedColorScheme = model.PredefinedColorScheme;
                newLayoutStyleModel.Basic = model.Basic;
                newLayoutStyleModel.Text = model.Text;
                newLayoutStyleModel.Elements = model.Elements;
                _MedORDB.LayoutStyles.AddObject(newLayoutStyleModel);
            }
            var success = _MedORDB.SaveChanges();
            return success;
        }
        #endregion

        #region MedicalInfoAndHistory
        public DataModel.PediatricMedicalModel GetMedicalInfoAndHistoryByPatientID(Guid PatientID)
        {
            var medicalInfoAndHistory = (from p in _MedORDB.PediatricMedicalInfoes
                                         where p.PatientID == PatientID
                                         select new DataModel.PediatricMedicalModel
                                         {
                                             MedicalInfoID = p.MedicalInfoID,
                                             BloodType = p.BloodType,
                                             Consanguinity = p.Consanguinity,
                                             PNormal = p.PNormal,
                                             PAlbuminuria = p.PAlbuminuria,
                                             PInfections = p.PInfections,
                                             PChildbirthThreat = p.PChildbirthThreat,
                                             POthers = p.POthers,
                                             PMedicines = p.PMedicines,
                                             CBTerm = p.CBTerm,
                                             CBVaginally = p.CBVaginally,
                                             CBCSection = p.CBCSection,
                                             CBForceps = p.CBForceps,
                                             BApgar = p.BApgar,
                                             BCyanosis = p.BCyanosis,
                                             BReanimation = p.BReanimation,
                                             BNeonatalWeight = p.BNeonatalWeight,
                                             BHeight = p.BHeight,
                                             BCranialPerimeter = p.BCranialPerimeter,
                                             BPlacenta = p.BPlacenta,
                                             NPGuthrie = p.NPGuthrie,
                                             NPPhysiologicalIcterus = p.NPPhysiologicalIcterus,
                                             NPNICU = p.NPNICU,
                                             NPCongenitalAbnormalities = p.NPCongenitalAbnormalities,
                                             NPBreastMilk = p.NPBreastMilk,
                                             NPArtificialMilk = p.NPArtificialMilk,
                                             NPArtificialMilkName = p.NPArtificialMilkName,
                                             PDFirstSmile = p.PDFirstSmile,
                                             PDSitUp = p.PDSitUp,
                                             PDSteps = p.PDSteps,
                                             PDSchooling = p.PDSchooling,
                                             PDFirstWord = p.PDFirstWord,
                                             PDStandUp = p.PDStandUp,
                                             PDDiurnalCleanliness = p.PDDiurnalCleanliness,
                                             DMilk = p.DMilk,
                                             DSolidFood = p.DSolidFood,
                                             DIron = p.DIron,
                                             DFluor = p.DFluor,
                                             DVitaminD = p.DVitaminD,
                                             DMultivitamin = p.DMultivitamin,
                                             AFood = p.AFood,
                                             AFoodTypes = p.AFoodTypes,
                                             AMedications = p.AMedications,
                                             AMedicationTypes = p.AMedicationTypes,
                                             AAsthma = p.AAsthma,
                                             AHives = p.AHives,
                                             AOthers = p.AOthers,
                                             CDWhoopingCough = p.CDWhoopingCough,
                                             CDMumps = p.CDMumps,
                                             CDMeasles = p.CDMeasles,
                                             CDRubella = p.CDRubella,
                                             CDScarlatine = p.CDScarlatine,
                                             CDChickenPox = p.CDChickenPox,
                                             CDRoseola = p.CDRoseola,
                                             CDOthers = p.CDOthers,
                                             MIGeneralNote = p.GeneralNote,
                                             ModifiedBy = p.ModifiedBy,
                                             ModifiedOn = p.ModifiedOn,
                                             MedicalHistoryID = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.MedicalHistoryID).FirstOrDefault(),
                                             MedicalHistory = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.MedicalHistory).FirstOrDefault(),
                                             SurgicalHistory = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.SurgicalHistory).FirstOrDefault(),
                                             FamilyHistory = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.FamilyHistory).FirstOrDefault(),
                                             MHGeneralNote = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.GeneralNote).FirstOrDefault(),
                                         }).FirstOrDefault();
            return medicalInfoAndHistory;
        }
        public Int32 UpdateMedicalInfoAndHistory(DataModel.PediatricMedicalModel model)
        {
            var editMedical = _MedORDB.PediatricMedicalInfoes.Where(x => x.PatientID == model.PatientID).FirstOrDefault();
            if (editMedical == null)
            {
                var newMedical = new DataAccess.PediatricMedicalInfo();
                newMedical.MedicalInfoID = Guid.NewGuid();
                newMedical.PatientID = model.PatientID;
                newMedical.BloodType = model.BloodType;
                newMedical.Consanguinity = model.Consanguinity;
                newMedical.PNormal = model.PNormal;
                newMedical.PAlbuminuria = model.PAlbuminuria;
                newMedical.PInfections = model.PInfections;
                newMedical.PChildbirthThreat = model.PChildbirthThreat;
                newMedical.POthers = model.POthers;
                newMedical.PMedicines = model.PMedicines;
                newMedical.CBTerm = model.CBTerm;
                newMedical.CBVaginally = model.CBVaginally;
                newMedical.CBCSection = model.CBCSection;
                newMedical.CBForceps = model.CBForceps;
                newMedical.BApgar = model.BApgar;
                newMedical.BCyanosis = model.BCyanosis;
                newMedical.BReanimation = model.BReanimation;
                newMedical.BNeonatalWeight = model.BNeonatalWeight;
                newMedical.BHeight = model.BHeight;
                newMedical.BCranialPerimeter = model.BCranialPerimeter;
                newMedical.BPlacenta = model.BPlacenta;
                newMedical.NPGuthrie = model.NPGuthrie;
                newMedical.NPPhysiologicalIcterus = model.NPPhysiologicalIcterus;
                newMedical.NPNICU = model.NPNICU;
                newMedical.NPCongenitalAbnormalities = model.NPCongenitalAbnormalities;
                newMedical.NPBreastMilk = model.NPBreastMilk;
                newMedical.NPArtificialMilk = model.NPArtificialMilk;
                newMedical.NPArtificialMilkName = model.NPArtificialMilkName;
                newMedical.PDFirstSmile = model.PDFirstSmile;
                newMedical.PDSitUp = model.PDSitUp;
                newMedical.PDSteps = model.PDSteps;
                newMedical.PDSchooling = model.PDSchooling;
                newMedical.PDFirstWord = model.PDFirstWord;
                newMedical.PDStandUp = model.PDStandUp;
                newMedical.PDDiurnalCleanliness = model.PDDiurnalCleanliness;
                newMedical.DMilk = model.DMilk;
                newMedical.DSolidFood = model.DSolidFood;
                newMedical.DIron = model.DIron;
                newMedical.DFluor = model.DFluor;
                newMedical.DVitaminD = model.DVitaminD;
                newMedical.DMultivitamin = model.DMultivitamin;
                newMedical.AFood = model.AFood;
                newMedical.AFoodTypes = model.AFoodTypes;
                newMedical.AMedications = model.AMedications;
                newMedical.AMedicationTypes = model.AMedicationTypes;
                newMedical.AAsthma = model.AAsthma;
                newMedical.AHives = model.AHives;
                newMedical.AOthers = model.AOthers;
                newMedical.CDWhoopingCough = model.CDWhoopingCough;
                newMedical.CDMumps = model.CDMumps;
                newMedical.CDMeasles = model.CDMeasles;
                newMedical.CDRubella = model.CDRubella;
                newMedical.CDScarlatine = model.CDScarlatine;
                newMedical.CDChickenPox = model.CDChickenPox;
                newMedical.CDRoseola = model.CDRoseola;
                newMedical.CDOthers = model.CDOthers;
                newMedical.GeneralNote = model.MIGeneralNote;
                newMedical.ModifiedBy = model.ModifiedBy;
                newMedical.ModifiedOn = model.ModifiedOn;
                _MedORDB.PediatricMedicalInfoes.AddObject(newMedical);
            }
            else
            {
                editMedical.BloodType = model.BloodType;
                editMedical.Consanguinity = model.Consanguinity;
                editMedical.PNormal = model.PNormal;
                editMedical.PAlbuminuria = model.PAlbuminuria;
                editMedical.PInfections = model.PInfections;
                editMedical.PChildbirthThreat = model.PChildbirthThreat;
                editMedical.POthers = model.POthers;
                editMedical.PMedicines = model.PMedicines;
                editMedical.CBTerm = model.CBTerm;
                editMedical.CBVaginally = model.CBVaginally;
                editMedical.CBCSection = model.CBCSection;
                editMedical.CBForceps = model.CBForceps;
                editMedical.BApgar = model.BApgar;
                editMedical.BCyanosis = model.BCyanosis;
                editMedical.BReanimation = model.BReanimation;
                editMedical.BNeonatalWeight = model.BNeonatalWeight;
                editMedical.BHeight = model.BHeight;
                editMedical.BCranialPerimeter = model.BCranialPerimeter;
                editMedical.BPlacenta = model.BPlacenta;
                editMedical.NPGuthrie = model.NPGuthrie;
                editMedical.NPPhysiologicalIcterus = model.NPPhysiologicalIcterus;
                editMedical.NPNICU = model.NPNICU;
                editMedical.NPCongenitalAbnormalities = model.NPCongenitalAbnormalities;
                editMedical.NPBreastMilk = model.NPBreastMilk;
                editMedical.NPArtificialMilk = model.NPArtificialMilk;
                editMedical.NPArtificialMilkName = model.NPArtificialMilkName;
                editMedical.PDFirstSmile = model.PDFirstSmile;
                editMedical.PDSitUp = model.PDSitUp;
                editMedical.PDSteps = model.PDSteps;
                editMedical.PDSchooling = model.PDSchooling;
                editMedical.PDFirstWord = model.PDFirstWord;
                editMedical.PDStandUp = model.PDStandUp;
                editMedical.PDDiurnalCleanliness = model.PDDiurnalCleanliness;
                editMedical.DMilk = model.DMilk;
                editMedical.DSolidFood = model.DSolidFood;
                editMedical.DIron = model.DIron;
                editMedical.DFluor = model.DFluor;
                editMedical.DVitaminD = model.DVitaminD;
                editMedical.DMultivitamin = model.DMultivitamin;
                editMedical.AFood = model.AFood;
                editMedical.AFoodTypes = model.AFoodTypes;
                editMedical.AMedications = model.AMedications;
                editMedical.AMedicationTypes = model.AMedicationTypes;
                editMedical.AAsthma = model.AAsthma;
                editMedical.AHives = model.AHives;
                editMedical.AOthers = model.AOthers;
                editMedical.CDWhoopingCough = model.CDWhoopingCough;
                editMedical.CDMumps = model.CDMumps;
                editMedical.CDMeasles = model.CDMeasles;
                editMedical.CDRubella = model.CDRubella;
                editMedical.CDScarlatine = model.CDScarlatine;
                editMedical.CDChickenPox = model.CDChickenPox;
                editMedical.CDRoseola = model.CDRoseola;
                editMedical.CDOthers = model.CDOthers;
                editMedical.GeneralNote = model.MIGeneralNote;
                editMedical.ModifiedBy = model.ModifiedBy;
                editMedical.ModifiedOn = model.ModifiedOn;
            }


            var editMedicalHistory = _MedORDB.PediatricMedicalHistories.Where(x => x.PatientID == model.PatientID).FirstOrDefault();
            if (editMedicalHistory == null)
            {
                var newMedicalHistory = new DataAccess.PediatricMedicalHistory();
                newMedicalHistory.MedicalHistoryID = Guid.NewGuid();
                newMedicalHistory.PatientID = model.PatientID;
                newMedicalHistory.MedicalHistory = model.MedicalHistory;
                newMedicalHistory.SurgicalHistory = model.SurgicalHistory;
                newMedicalHistory.FamilyHistory = model.FamilyHistory;
                newMedicalHistory.GeneralNote = model.MHGeneralNote;
                newMedicalHistory.ModifiedBy = model.ModifiedBy;
                newMedicalHistory.ModifiedOn = model.ModifiedOn;
                _MedORDB.PediatricMedicalHistories.AddObject(newMedicalHistory);
            }
            else
            {
                editMedicalHistory.MedicalHistory = model.MedicalHistory;
                editMedicalHistory.SurgicalHistory = model.SurgicalHistory;
                editMedicalHistory.FamilyHistory = model.FamilyHistory;
                editMedicalHistory.GeneralNote = model.MHGeneralNote;
                editMedicalHistory.ModifiedBy = model.ModifiedBy;
                editMedicalHistory.ModifiedOn = model.ModifiedOn;
            }

            var success = _MedORDB.SaveChanges();
            return success;
        }
        #endregion

        #region ViewCreateModify
        public Int32 AddNewCreateModify(DataModel.ViewCreateModifyModel model)
        {
            DataAccess.CreateModifyView newViewCreateModify = new DataAccess.CreateModifyView();
            newViewCreateModify.CreateModifyViewID = Guid.NewGuid();
            newViewCreateModify.UserID = model.UserID;
            newViewCreateModify.TableName = model.TableName;
            newViewCreateModify.CreatedByView = model.ViewCreatedBy;
            newViewCreateModify.CreatedOnView = model.ViewCreatedOn;
            newViewCreateModify.ModifiedByView = model.ViewModifiedBy;
            newViewCreateModify.ModifiedOnView = model.ViewModifiedOn;
            _MedORDB.CreateModifyViews.AddObject(newViewCreateModify);
            var success = _MedORDB.SaveChanges();

            return success;
        }
        public Int32 UpdateCreateModify(DataModel.ViewCreateModifyModel model)
        {
            var editCreateModifyView = _MedORDB.CreateModifyViews.Where(x => x.UserID == model.UserID && x.TableName == model.TableName).FirstOrDefault();
            editCreateModifyView.CreatedByView = model.ViewCreatedBy;
            editCreateModifyView.CreatedOnView = model.ViewCreatedOn;
            editCreateModifyView.ModifiedByView = model.ViewModifiedBy;
            editCreateModifyView.ModifiedOnView = model.ViewModifiedOn; 
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public DataModel.ViewCreateModifyModel GetViewCreateModifyByUserAndTableName(Guid UserID, String TableName)
        {
            var viewCreateModifyModel = (from cm in _MedORDB.CreateModifyViews
                                         where cm.UserID == UserID && cm.TableName == TableName
                                         select new DataModel.ViewCreateModifyModel
                                         {
                                             UserID = cm.UserID,
                                             TableName = cm.TableName,
                                             ViewCreatedBy = cm.CreatedByView,
                                             ViewCreatedOn = cm.CreatedOnView,
                                             ViewModifiedBy = cm.ModifiedByView,
                                             ViewModifiedOn = cm.ModifiedOnView,
                                             //SecurityModel = (from u in _MedORDB.Users
                                             //                 where u.UserID == UserID
                                             //                 from r in _MedORDB.Roles
                                             //                 where r.RoleID == u.RoleID
                                             //                 from p in _MedORDB.Permissions
                                             //                 where p.PermissionName == "Appointments"
                                             //                 from s in _MedORDB.Securities
                                             //                 where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                             //                 select new DataModel.SecurityModel
                                             //                 {
                                             //                     View = s.View,
                                             //                     Create = s.Create,
                                             //                     Edit = s.Edit,
                                             //                     Delete = s.Delete
                                             //                 }).FirstOrDefault()
                                         }).FirstOrDefault();
            return viewCreateModifyModel;
        }
        public Boolean GetViewCreateModifyExist(Guid UserID, String TableName)
        {
            var viewCreateModifyExist = (from cm in _MedORDB.CreateModifyViews
                                         where cm.UserID == UserID && cm.TableName == TableName
                                         select cm).Any();

            return viewCreateModifyExist;
        }  
                
        #endregion
    }
}
