﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class DropDownModel
    {
        public Guid DropDownID { get; set; }
        public String Type { get; set; }
        public String Value { get; set; }
    }
}
