﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class SecurityModel
    {
        public Guid SecurityID { get; set; }
        public Guid RoleID { get; set; }
        public Guid PermissionID { get; set; }
        public Boolean View { get; set; }
        public Boolean MedicalHistoryView { get; set; }
        public Boolean Create { get; set; }
        public Boolean Edit { get; set; }
        public Boolean Delete { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public String PermissionName { get; set; }
    }
}
