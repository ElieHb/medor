﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class LayoutStyleModel
    {
        public Guid LayoutStyleID { get; set; }
        public Guid UserID { get; set; }
        public String Rtl { get; set; }
        public String Layout { get; set; }
        public String Header { get; set; }
        public String Footer { get; set; }
        public String Background { get; set; }
        public String PredefinedColorScheme { get; set; }
        public String Basic { get; set; }
        public String Text { get; set; }
        public String Elements { get; set; }
    }
}
