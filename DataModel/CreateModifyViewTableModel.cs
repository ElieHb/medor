﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class CreateModifyViewTableModel
    {
        public String table { get; set; }
        public Boolean vcreatedBy { get; set; }
        public Boolean vcreatedOn { get; set; }
        public Boolean vmodifiedBy { get; set; }
        public Boolean vmodifiedOn { get; set; }
    }
}
