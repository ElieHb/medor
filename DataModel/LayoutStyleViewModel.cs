﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class LayoutStyleViewModel
    {
        public String rtl { get; set; }
        public String layoutBoxed { get; set; }
        public String bgStyle { get; set; }
        public String headerDefault { get; set; }
        public String footerDefault { get; set; }
        public String useLess { get; set; }
        public String baseColor { get; set; }
        public String textColor { get; set; }
        public String badgeColor { get; set; }
        public String skinClass { get; set; }
    }
}
