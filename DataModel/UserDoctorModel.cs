﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class UserDoctorModel
    {
        public Guid UserDoctorID { get; set; }
        public Guid UserID { get; set; }
        public Guid DoctorID { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
