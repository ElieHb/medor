﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class  InvoicesModel
    {
        public Guid InvoiceID { get; set; }
        public Guid PatientID { get; set; }
        public Guid DoctorID { get; set; }
        public decimal Total { get; set; }
        public int VAT { get; set; }
        public decimal SubTotal { get; set; }
        public DateTime CreatedON { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? ModifiedON { get; set; }
        public Guid? ModifiedBy { get; set; }
    }
}
