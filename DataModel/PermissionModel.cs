﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class PermissionModel
    {
        public Guid PermissionID { get; set; }
        public String PermissionName { get; set; }

        public SecurityModel SecurityModel { get; set; }
        public Boolean View { get; set; }
        public Boolean Create { get; set; }
        public Boolean Edit { get; set; }
        public Boolean Delete { get; set; }
    }
}
