﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class ViewCreateModifyModel
    {
        public Guid ViewCreateModifyID { get; set; }
        public Guid UserID { get; set; }
        public String TableName { get; set; }
        public Boolean ViewCreatedBy { get; set; }
        public Boolean ViewCreatedOn { get; set; }
        public Boolean ViewModifiedBy { get; set; }
        public Boolean ViewModifiedOn { get; set; }
    }
}
