﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class AppointmentModel
    {
        public Guid AppointmentID { get; set; }
        public Guid? PatientID { get; set; }
        public Guid? DoctorID { get; set; }
        public Guid ClinicID { get; set; }
        public String Title { get; set; }
        public Boolean AllDay { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public String ClassName { get; set; }
        public String Note { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public String ModifiedByName { get; set; }

        public String id { get; set; }
        public String title { get; set; }
        public String start { get; set; }
        public String end { get; set; }
        public Boolean allDay { get; set; }
        public String className { get; set; }

        public SecurityModel SecurityModel { get; set; }
    }
}
