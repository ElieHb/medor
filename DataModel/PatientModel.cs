﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class PaitentModel
    {
        public Guid PatientID { get; set; }
        public String FullName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }
        public Boolean Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public String DD { get; set; }
        public String MM { get; set; }
        public String YYYY { get; set; }
        public String PatientEmail { get; set; }
        public String MobilePhone { get; set; }
        public String HomePhone { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String Country { get; set; }
        public Byte[] Image { get; set; }
        public Guid CreatedBy { get; set; }
        public String CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public String ModifiedByName { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public SecurityModel SecurityModel { get; set; }
    }
}

