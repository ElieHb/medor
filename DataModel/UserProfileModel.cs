﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class UserProfileModel
    {
        public int UserID { get; set; }
        public String Username { get; set; }
        public String EmailID { get; set; }
    }
}
