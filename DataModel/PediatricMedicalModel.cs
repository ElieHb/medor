﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class PediatricMedicalModel
    {
        public Guid MedicalInfoID { get; set; }
        public Guid PatientID { get; set; }
        public Guid? BloodType { get; set; } 
        public Boolean Consanguinity { get; set; }
        public Boolean PNormal { get; set; }
        public Boolean PAlbuminuria { get; set; }
        public Boolean PHTA { get; set; }
        public Boolean PInfections { get; set; }
        public Boolean PChildbirthThreat { get; set; }
        public String POthers { get; set; }
        public String PMedicines { get; set; }
        public String CBTerm { get; set; } // limit 15 char
        public Boolean CBVaginally { get; set; }
        public Boolean CBCSection { get; set; }
        public Boolean CBForceps { get; set; }
        public String BApgar { get; set; } // limit 15 char
        public Boolean BCyanosis { get; set; } 
        public String BReanimation { get; set; } // limit 15 char
        public String BNeonatalWeight { get; set; } //limit 15 char
        public String BHeight { get; set; } // limit 15 char
        public String BCranialPerimeter { get; set; } // limit 15 char
        public String BPlacenta { get; set; } // limit 15 char
        public String NPGuthrie { get; set; }
        public Boolean NPPhysiologicalIcterus { get; set; }
        public String NPNICU { get; set; }
        public String NPCongenitalAbnormalities { get; set; }
        public Boolean NPBreastMilk { get; set; }
        public Boolean NPArtificialMilk { get; set; }
        public String NPArtificialMilkName { get; set; } // limit 50 char
        public String PDFirstSmile { get; set; } // limit 15 char
        public String PDSitUp { get; set; } // limit 15 char
        public String PDSteps { get; set; } // limit 15 char
        public String PDSchooling { get; set; } // limit 15 char
        public String PDFirstWord { get; set; } // limit 15 char
        public String PDStandUp { get; set; } // limit 15 char
        public String PDDiurnalCleanliness { get; set; } // limit 15 char
        public String DMilk { get; set; } // limit 25 char
        public String DSolidFood { get; set; } // limit 15 char
        public Boolean DIron { get; set; }
        public Boolean DFluor { get; set; }
        public Boolean DVitaminD { get; set; }
        public Boolean DMultivitamin { get; set; }
        public Boolean AFood { get; set; }
        public String AFoodTypes { get; set; }
        public Boolean AMedications { get; set; }
        public String AMedicationTypes { get; set; }
        public Boolean AAsthma { get; set; }
        public Boolean AHives { get; set; }
        public String AOthers { get; set; }
        public Boolean CDWhoopingCough { get; set; }
        public Boolean CDMumps { get; set; }
        public Boolean CDMeasles { get; set; }
        public Boolean CDRubella { get; set; }
        public Boolean CDScarlatine { get; set; }
        public Boolean CDChickenPox { get; set; }
        public Boolean CDRoseola { get; set; }
        public String CDOthers { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public String MIGeneralNote { get; set; }

        public Guid MedicalHistoryID { get; set; }
        public String MedicalHistory { get; set; }
        public String SurgicalHistory { get; set; }
        public String FamilyHistory { get; set; }
        public String MHGeneralNote { get; set; }

        public String FirstName { get; set; }
        public String LastName { get; set; }
    }
}
