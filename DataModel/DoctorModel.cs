﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class DoctorModel
    {
        public Guid DoctorID { get; set; }
        public String FullName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public Boolean Gender { get; set; }
        public String MobilePhone { get; set; }
        public String WorkPhone { get; set; }
        public String Email { get; set; }
        public Guid? Specialty { get; set; }
        public Guid? SpecialtyID { get; set; }
        public String SpecialtyName { get; set; }
        public DateTime CreatedOn { get; set; }
        public String CreatedByName { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public String ModifiedByName { get; set; }

        public IEnumerable<DropDownModel> DropDownModel { get; set; }
        public SecurityModel SecurityModel { get; set; }
    }
}
