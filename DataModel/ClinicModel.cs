﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class ClinicModel
    {
        public Guid ClinicID { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String Country { get; set; }
        public String Phone { get; set; }
        public DateTime CreatedOn { get; set; }
        public String CreatedByName { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public String ModifiedByName { get; set; }

        public SecurityModel SecurityModel { get; set; }
    }
}
